import mariadb
import datetime
import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

class mariadb_part:
    def __init__(self,DATABASE, MARIADB_SERVER, MARIADB_USER, MARIADB_PASSWD, TABLE_PREFIX, TIMESTAMP_FIELD_NAME):
        self.DATABASE = DATABASE
        self.MARIADB_SERVER = MARIADB_SERVER
        self.MARIADB_USER = MARIADB_USER
        self.MARIADB_PASSWD = MARIADB_PASSWD
        self.TIMESTAMP_FIELD_NAME = TIMESTAMP_FIELD_NAME

    def connect(self):
        self.conn = mariadb.connect(user=self.MARIADB_USER, password=self.MARIADB_PASSWD,
                                             host=self.MARIADB_SERVER, database=self.DATABASE)
        self.cursor = self.conn.cursor()
        return self.conn,self.cursor

    def get_tables_columns(self, cursor,database):
        query = f'SHOW TABLES FROM `{database}`'
        cursor.execute(query)
        tables = []
        for table in cursor.fetchall():
            # print(table[0])
            tables.append(table[0])
        dict_tables = {}
        for table in tables:
            if table not in dict_tables.keys():
                dict_tables[table] = []
            query = f'SHOW COLUMNS FROM `{database}`.`{table}`'
            cursor.execute(query)
            for row in cursor.fetchall():
                dict_tables[table].append(row[0])
        return dict_tables

    def get_schema(self, cursor,database):
        query = 'SHOW DATABASES'
        cursor.execute(query)
        schema = {}
        for row in cursor.fetchall():
            schema[row[0]] = []
        for database in schema:
            schema[database] = self.get_tables_columns(self,cursor,database)
        return schema

    def create_tables_sql(self,tables_dict):
        """

        :param tables_dict: {'table name':[('column name 1','type'),('colunm name 2','type')]}
        :return: list of sql commands to create tables
        """
        table_create_sql = []
        logger.info(f"Create table {self.DATABASE} {tables_dict.keys()}")
        for table_name in tables_dict.keys():
            columns_with_types = ",\n".join([f"`{name}` {type}" for name, type in tables_dict[table_name]])
            table_create_sql.append(f"""CREATE TABLE IF NOT EXISTS `{self.DATABASE}`.`{table_name}` 
                                    ({columns_with_types}) ENGINE=ColumnStore;""")
        return table_create_sql

    def create_column_sql(self,database,table,name,type):
        logger.info(f"Add column: `{table}` in `{database}`")
        return f"ALTER TABLE `{table}`.`{database}` ADD COLUMN IF NOT EXISTS   `{name}` {type};"

    def insert_data(self):
        pass

    def close_connection(self):
        pass
