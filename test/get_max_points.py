import asyncio
import logging
import sys
from argparse import ArgumentParser
from datetime import datetime
import asyncio_dgram
import traceback
import io
from pympler import asizeof
from array import array

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

BUFFER_LIST = list()
GLOBAL_LIST = list()
BUFFER_COUNT = 0
#GLOBAL_ARRAY = array('u',[])

###############################################################
# ACTIVE FUNCTIONS

def cmd_arguments():
    parser = ArgumentParser()
    parser.add_argument("--server-ip", dest="serv_ip",
                        help="server ip address to bind, 127.0.0.1 if not defined", required=False, default='127.0.0.1')
    parser.add_argument("--server-port", dest="serv_port",
                        help="udp server port to bind, default 9150", required=False, type=int, default=9100)
    parser.add_argument("-l", "--log-level", dest="loglevel",
                        help="log level, DEBUG, INFO, WARNING, ERROR, CRITICAL, default NOTSET.", required=False, default='NOTSET')
    parser.add_argument("--cmd-ip", dest="cmd_ip",
                        help="Command interface ip address to bind, 127.0.0.1 if not defined.", required=False,
                        default='127.0.0.1')
    parser.add_argument("--cmd-port", dest="cmd_port",
                        help="Command interface port to bind, 9101 if not defined", required=False,
                        default='9101')
    args = parser.parse_args()
    SERV_IP = vars(args)['serv_ip']
    SERV_PORT = vars(args)['serv_port']
    LOGLEVEL = vars(args)['loglevel']
    CMD_IP = vars(args)['cmd_ip']
    CMD_PORT = vars(args)['cmd_port']
    return SERV_IP,SERV_PORT,LOGLEVEL,CMD_IP,CMD_PORT

async def print_list_count_and_size():
    global BUFFER_LIST
    #header csv
    header = ",".join(["time",
              "BUFFER_LIST elements",
              "BUFFER_LIST pympler size",
              "BUFFER_LIST sys size"
              ])
    print(header)
    while True:
        await asyncio.sleep(10)
        line = ",".join([str(datetime.now().strftime("%Y.%m.%d %H:%M:%S")),
                         str(BUFFER_COUNT),
                         str(asizeof.asizeof(BUFFER_LIST)),
                         str(sys.getsizeof(BUFFER_LIST)),
                         ])
        print(line)

async def udp_server(IP,PORT):
    stream = await asyncio_dgram.bind((IP, PORT))
    global BUFFER_LIST
    global BUFFER_COUNT
    print(f"Serving on {stream.sockname}")
    while True:
        data, remote_addr = await stream.recv()
        #print(f"Received {data} from {remote_addr}")
        temp = data.decode().strip().splitlines()
        #BUFFER_LIST.append(temp)
        BUFFER_COUNT = BUFFER_COUNT + len(temp)
        #GLOBAL_ARRAY.append("\n")
        #GLOBAL_ARRAY = GLOBAL_ARRAY + array('u',data.decode().strip())

async def command_listener(reader, writer):
    data = await reader.read(100)
    command = data.decode()
    addr = writer.get_extra_info('peername')

    print(f"Received {command!r} from {addr!r}")

    result = str()
    ######################################
    # REDIRECT OUTPUT
    # keep a named handle on the prior stdout
    old_stdout = sys.stdout
    # keep a named handle on io.StringIO() buffer
    new_stdout = io.StringIO()
    # Redirect python stdout into the builtin io.StringIO() buffer
    sys.stdout = new_stdout

    try:
        exec(str(command).strip())
        result = sys.stdout.getvalue().strip()
    except Exception as e:
        result = str(traceback.format_exc())

    # put stdout back to normal
    sys.stdout = old_stdout
    # END OF REDIRECT
    ######################################
    print(f"Send: {result}")
    writer.write(str.encode(f"Output: {result}\n"))
    await writer.drain()

    print("Close the connection")
    writer.close()

async def from_buffer_to_memory():
    global BUFFER_LIST
    global GLOBAL_LIST
    temp = BUFFER_LIST.copy()
    while True:
        await asyncio.sleep(30)
        GLOBAL_LIST = GLOBAL_LIST + temp
        for i in range(len(temp)):
            BUFFER_LIST.pop(0)

async def main():
    SERV_IP,SERV_PORT,LOGLEVEL,CMD_IP,CMD_PORT = cmd_arguments()
    logger.setLevel(LOGLEVEL)
    cmd_server = await asyncio.start_server(command_listener, CMD_IP, CMD_PORT)
    await asyncio.gather(
        udp_server(SERV_IP,SERV_PORT),
        print_list_count_and_size(),
        #cmd_server.serve_forever(),
        #from_buffer_to_memory()
    )

if __name__ == "__main__":
    asyncio.run(main())


###########################################################
# DISABLED FUNTIONS
#async def simple_add_message_to_list(reader,writer):
#    data = await reader.readline()
#    message = data.decode()
#    GLOBAL_LIST.append(message)