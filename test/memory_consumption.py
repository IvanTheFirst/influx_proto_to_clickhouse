import sys
from pympler import asizeof
from datetime import datetime
from array import array

GLOBAL_LIST = list()
GLOBAL_KEY = int(1)
GLOBAL_DICT = dict()
GLOBAL_TUPLE = tuple()
STRING_TO_ADD = """As others have stated, sys.getsizeof only returns the size of the object structure that represents your 
        data. So if, for instance, you have a dynamic array that you keep adding elements to,"""
GLOBAL_ARRAY = array('u',[])
LIMIT = 1000

def get_size(obj):
    print(f'{datetime.now().strftime("%Y.%m.%d %H:%M:%S")}')
    print(f"Size by pympler: {asizeof.asizeof(obj)}")
    print(f"Size by sys: {sys.getsizeof(obj)}")

def add_to_dict():
    global GLOBAL_KEY
    global GLOBAL_DICT
    while GLOBAL_KEY < LIMIT:
        temp = """As others have stated, sys.getsizeof only returns the size of the object structure that represents your 
        data. So if, for instance, you have a dynamic array that you keep adding elements to,"""
        GLOBAL_DICT[GLOBAL_KEY] = temp
        GLOBAL_KEY = GLOBAL_KEY + 1
    get_size(GLOBAL_DICT)
    input('Press something to clear memory')

def add_to_list():
    global GLOBAL_LIST
    while len(GLOBAL_LIST) < LIMIT:
        temp = """As others have stated, sys.getsizeof only returns the size of the object structure that represents your 
        data. So if, for instance, you have a dynamic array that you keep adding elements to,"""
        GLOBAL_LIST.append(temp+str(len(GLOBAL_LIST)))
    get_size(GLOBAL_LIST)
    input('Press something to copy list to another')
    new_list = GLOBAL_LIST.copy()
    input('Next del GLOBAL_LIST')
    GLOBAL_LIST = None
    get_size(new_list)
    input('Exit')

def add_to_list_of_array():
    global GLOBAL_LIST
    while len(GLOBAL_LIST) < LIMIT:
        temp = STRING_TO_ADD + str(len(GLOBAL_LIST))
        GLOBAL_LIST.append(array('u',temp))
    get_size(GLOBAL_LIST)
    input('Exit')

class ManyVariables:
    def __init__(self,varname):
        global GLOBAL_KEY
        while GLOBAL_KEY < LIMIT:
            new_var = varname + str(GLOBAL_KEY)
            temp = """As others have stated, sys.getsizeof only returns the size of the object structure that represents your 
                    data. So if, for instance, you have a dynamic array that you keep adding elements to,"""
            setattr(self, new_var, temp)
            GLOBAL_KEY = GLOBAL_KEY + 1


def add_to_class():
    temp = ManyVariables("a")
    print(f'{datetime.now().strftime("%Y.%m.%d %H:%M:%S")}. Size is {asizeof.asizeof(temp)}')
    input('Press something')

def add_to_tuple():
    global GLOBAL_TUPLE
    while len(GLOBAL_TUPLE) < LIMIT:
        temp = """As others have stated, sys.getsizeof only returns the size of the object structure that represents your 
           data. So if, for instance, you have a dynamic array that you keep adding elements to,"""
        GLOBAL_TUPLE = GLOBAL_TUPLE + (temp,)
    get_size(GLOBAL_TUPLE)
    input('Press something')

def add_to_array():
    global GLOBAL_ARRAY
    while len(GLOBAL_ARRAY) < LIMIT:
        temp = STRING_TO_ADD + str(len(GLOBAL_LIST))
        GLOBAL_ARRAY.append("\n")
        GLOBAL_ARRAY = GLOBAL_ARRAY + array('u',temp)
    get_size(GLOBAL_ARRAY)
    input('Exit')

#async def main():
#    await asyncio.gather(
#        add_to_list_and_dict()
#    )

if __name__ == "__main__":
    LIMIT = int(sys.argv[1])
    if (sys.argv[2] == "-l"): add_to_list()
    elif (sys.argv[2] == "-d"): add_to_dict()
    elif (sys.argv[2] == "-c"): add_to_class()
    elif (sys.argv[2] == "-t"): add_to_tuple()
    elif (sys.argv[2] == "-la"): add_to_list_of_array()
    elif (sys.argv[2] == "-a"): add_to_array()