import sqlalchemy
from sqlalchemy.dialects import registry
import sqlalchemy_clickhouse as sqlalchemy_clickhouse
print(sqlalchemy_clickhouse.VERSION)

dbname='monitoring'
dbhost='172.18.14.144'
dbuser='default'

registry.register("clickhouse", "sqlalchemy_clickhouse.base", "dialect")

engine = sqlalchemy.create_engine(f"clickhouse://{dbuser}@{dbhost}/{dbname}")
a = engine.execute('show users')
print(a.fetchall())
