from argparse import ArgumentParser
import sys
import toml
import re

SAMPLE_CONFIG = """
## DEBUG, INFO, WARNING, ERROR, CRITICAL, default NOTSET
log_level = "NOTSET"
## how many message routers(thread to route/parse are used) per data_format and server
routers_count = 4

[[server.udp]]
    listen = "0.0.0.0:9100"
#   # data_format maybe influx_proto only for now
    data_format = "influx_proto"
#   # use python re format for 'measurement'
    measurement = ".*"
    database = "monitoring"
#   # Table engine for created tables
    table-engine = "ReplicatedMergeTree"
#   # TTL for created tables in clickhouse in days
    ttl = 2

[[server.nats]]
   listen = "0.0.0.0:4222"
#   # if using server_nats_emulation_tls
   ca = "./CAcert.pem"
   cert = "./certificate.pem"
   key = "./key.pem"

[[server.nats.routing_rules]]
    data_format = "influx_proto"
    subject = "MONITORING.system_per_second"
    measurement = ".*"
    database = "monitoring"
#   # Table engine for created tables
    table-engine = "ReplicatedMergeTree"
#   # TTL for created tables in clickhouse in days
    ttl = 2

[clickhouse]
## clickhouse connection scheme secure True/False
secure = true
## clickhouse host and port
host = "127.0.0.1:9443"
login = "some login"
password = "some password"
## clickhouse cluster to create tables
cluster = "monitoring"
## total number of writers to clickhouse
writers_count = 1
"""

# Server Types
SERVER_UDP = 'udp'
SERVER_NATS = 'nats'

# data formats
DATA_INFLUX_PROTO = 'influx_proto'
SUPPORTED_DATA_FORMATS = [DATA_INFLUX_PROTO]

class Configuration:
    def __init__(self):
        #self.dict_of_buffers = dict_of_buffers
        pass

    def create_config(self,create_config_path) -> None:
        f = open(create_config_path,mode="w")
        f.write(SAMPLE_CONFIG)
        f.close()

    def read(self) -> dict:
        parser = ArgumentParser()
        parser.add_argument("-c","--config", dest="config",
                            help="path to configuration file",
                            required=False)
        parser.add_argument("--create_config", dest="create_config",
                            help="Create default config in path specified",
                            required=False)
        args = parser.parse_args()
        #print(vars(args).values())
        if not any(vars(args).values()):
            parser.print_help()
            sys.exit()
        configuration_file = vars(args)['config']
        create_config_path = vars(args)['create_config']
        if create_config_path:
            self.create_config(create_config_path)
            sys.exit()
        output = toml.load(configuration_file)
        ######
        ## test for config settings
        if SERVER_NATS in output['server'].keys():
            for nats_server_record in output['server'][SERVER_NATS]:
                if 'ca' in nats_server_record.keys():
                    try:
                        f = open(nats_server_record['ca'])
                        f.close()
                    except Exception as e:
                        sys.exit(f"Can't read file {nats_server_record['ca']}")
                if 'cert' in nats_server_record.keys():
                    try:
                        f = open(nats_server_record['cert'])
                        f.close()
                    except Exception as e:
                        sys.exit(f"Can't read file {nats_server_record['cert']}")
                if 'key' in nats_server_record.keys():
                    try:
                        f = open(nats_server_record['key'])
                        f.close()
                    except Exception as e:
                        sys.exit(f"Can't read file {nats_server_record['key']}")

                if 'routing_rules'not in nats_server_record.keys():
                    sys.exit(f"No routing rules for NATS server {nats_server_record} is defined")
                else:
                    for rule_record in nats_server_record['routing_rules']:
                        if 'data_format' not in rule_record.keys():
                            sys.exit(f"There is no data_format in routing: {rule_record}")
                        else:
                            if rule_record['data_format'] not in SUPPORTED_DATA_FORMATS:
                                sys.exit(f"unsupported data_format in routing: {rule_record}")

                        if 'measurement' not in rule_record.keys():
                            sys.exit(f"There is no measurement in routing: {rule_record}")
                        try:
                            rule_record['measurement'] = re.compile(rule_record['measurement'])
                        except Exception as e:
                            sys.exit(f"Can't compile template for measurement {rule_record['measurement']}")

                        if 'subject' not in rule_record.keys():
                            sys.exit(f"There is no subject in routing: {rule_record}")
                        try:
                            rule_record['subject'] = re.compile(rule_record['subject'])
                        except Exception as e:
                            sys.exit(f"Can't compile template for subject {rule_record['subject']}")

                        if 'database' not in rule_record.keys():
                            sys.exit(f"There is no database in routing: {rule_record}")

        elif SERVER_UDP in output['server'].keys():
            for udp_server_record in output['server'][SERVER_UDP]:
                if 'data_format' not in udp_server_record.keys():
                    sys.exit(f"There is no data_format in server udp: {udp_server_record}")
                else:
                    if udp_server_record['data_format'] not in SUPPORTED_DATA_FORMATS:
                        sys.exit(f"unsupported data_format in server udp: {udp_server_record}")
        return output

