import asyncio
from datetime import datetime
from threading import Thread
import signal

GLOBAL_LIST = list()

async def append():
    print("append is running")
    global GLOBAL_LIST
    while True:
        length = len(GLOBAL_LIST)
        GLOBAL_LIST.append(len(GLOBAL_LIST))
        timestamp = str(datetime.now().strftime("%Y.%m.%d %H:%M:%S"))
        print(f'{timestamp}. Append add: {length}')
        await asyncio.sleep(0.001)

async def pop_async():
    print("pop is running")
    global GLOBAL_LIST
    while True:
        await asyncio.sleep(0.001)
        if len(GLOBAL_LIST) > 0:
            timestamp = str(datetime.now().strftime("%Y.%m.%d %H:%M:%S"))
            print(f"{timestamp}. Remove top element :{GLOBAL_LIST.pop(0)}. Length: {len(GLOBAL_LIST)}")

def start_background_loop(loop: asyncio.AbstractEventLoop) -> None:
    asyncio.set_event_loop(loop)
    loop.run_forever()

def main() -> None:
    loop = asyncio.new_event_loop()
    t = Thread(target=start_background_loop, args=(loop,), daemon=True)
    t.start()

    asyncio.run_coroutine_threadsafe( pop_async(), loop)
    asyncio.run(append())


if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()