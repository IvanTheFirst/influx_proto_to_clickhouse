from threading import Thread, RLock
import asyncio
import janus
import time
import logging
from config import Configuration
import signal
import concurrent.futures

"""
# Server Types
SERVER_UDP = 'udp'
SERVER_NATS = 'nats'

# data formats
DATA_INFLUX_PROTO = 'influx_proto'

BUFFER_QUEUES = {SERVER_UDP:{DATA_INFLUX_PROTO: janus.Queue().sync_q},
                 SERVER_NATS:{DATA_INFLUX_PROTO: janus.Queue().async_q}}
"""


def read_from_queue(queue: janus.SyncQueue[str]) -> None:
    print("start read from queue")
    while True:
        print(queue.get())
        time.sleep(1)

class ServerNatsAIO(asyncio.Protocol):
    def __init__(self,
                 cert_file: str,
                 ca_file: str,
                 key_file: str,
                 queue: janus.AsyncQueue[str]
                 ):
        print("start server")
        self.cert_file = cert_file
        self.ca_file = ca_file
        self.key_file = key_file
        self.queue = queue

    def connection_made(self, transport):
        peername = transport.get_extra_info('peername')
        print('Connection from {}'.format(peername))
        self.transport = transport

    def data_received(self, data):
        message = data.decode()
        print('Data received: {!r}'.format(message))

async def server_nats_async(host_port: str, cert: str, ca: str, key: str, queue: janus.AsyncQueue[str]):
    host = host_port.split(':')[0]
    port = host_port.split(':')[0]
    loop = asyncio.get_running_loop()

    server = await loop.create_server(
        lambda: ServerNatsAIO(cert,ca,key,queue),
        host, int(port))

    async with server:
        await server.serve_forever()

def start_background_loop(loop: asyncio.AbstractEventLoop) -> None:
    asyncio.set_event_loop(loop)
    loop.run_forever()

async def nats_server(host:str,port: str, cert: str, ca: str, key: str, queue: janus.AsyncQueue[str]):
    loop = asyncio.get_running_loop()
    server = await loop.create_server(
        lambda: ServerNatsAIO(cert,ca,key,queue),
        host, int(port))

    async with server:
        await server.serve_forever()

async def main(configuration:dict) -> None:

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)
    logger = logging.getLogger(__name__)

    if configuration['log_level'] == 'DEBUG':
        logger.setLevel(logging.DEBUG)
    elif configuration['log_level'] == 'INFO':
        logger.setLevel(logging.INFO)
    elif configuration['log_level'] == 'ERROR':
        logger.setLevel(logging.ERROR)
    elif configuration['log_level'] == 'CRITICAL':
        logger.setLevel(logging.CRITICAL)
    elif configuration['log_level'] == 'WARNING':
        logger.setLevel(logging.WARNING)
    else:
        logger.setLevel(logging.NOTSET)

    queue: janus.Queue[int] = janus.Queue()
    loop = asyncio.get_running_loop()

    #print(configuration)
    servers = []
    for server_cfg in configuration['server']['nats']:
        #print(server_cfg)
        ca_file = server_cfg['ca']
        key_file = server_cfg['key']
        cert_file = server_cfg['certificate']
        host = server_cfg['listen'].split(':')[0]
        port = server_cfg['listen'].split(':')[1]

        #asyncio.run_coroutine_threadsafe(nats_server(host,port,ca_file,key_file,cert_file,queue.async_q), loop )
        server = await loop.create_server(
            lambda: ServerNatsAIO(cert_file, ca_file, key_file, queue),
            host, int(port))

        servers.append(server)

    loop.run_in_executor(None, read_from_queue, queue.sync_q )
    async with [servers]:
        await asyncio.gather([servers])

if __name__ == "__main__":
    get_configuration = Configuration()
    configuration = get_configuration.read()
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    asyncio.run(main(configuration))
