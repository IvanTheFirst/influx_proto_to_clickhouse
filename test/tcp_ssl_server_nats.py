from threading import Thread, RLock
import queue
import time
import logging
from config import Configuration
import signal
import selectors
import socket
import ssl
import sys
import traceback
import os


BUFFER_BYTES = bytearray()
"""
# Server Types
SERVER_UDP = 'udp'
SERVER_NATS = 'nats'

# data formats
DATA_INFLUX_PROTO = 'influx_proto'

BUFFER_QUEUES = {SERVER_UDP:{DATA_INFLUX_PROTO: janus.Queue().sync_q},
                 SERVER_NATS:{DATA_INFLUX_PROTO: janus.Queue().async_q}}
"""

class NatsServer():
    def __init__(self,
                 host:str,
                 port:int,
                 buffer_size: int,
                 secure:bool=False,
                 ca_cert_file:str="",
                 key_file:str="",
                 blocking:bool=False
                 ):
        self.sel = selectors.DefaultSelector()
        self.host = host
        self.port = port
        self.buffer_size = buffer_size
        self.secure = secure
        self.ca_cert_file = ca_cert_file
        self.key_file = key_file
        self.blocking = blocking

    def start(self):
        try:
            print("start tcp server")
            sock = socket.socket()
            sock.bind((self.host, self.port))
            # sock.listen(100)
            sock.listen()
            sock.setblocking(self.blocking)
            self.sel.register(sock, selectors.EVENT_READ, self.accept)
        except:
            sys.excepthook(*sys.exc_info())

        while True:
            events = self.sel.select()
            for key, mask in events:
                callback = key.data
                callback(key.fileobj, mask)

    def accept(self,sock, mask):
        conn, addr = sock.accept()  # Should be ready
        print('accepted', conn, 'from', addr)
        # приветствие NATS для включения TLS
        # включение TLS
        if self.secure:
            self.sel.register(conn, selectors.EVENT_READ, self.accept_secure)
        else:
            conn.setblocking(self.blocking )
            self.sel.register(conn, selectors.EVENT_READ, self.read)

    def accept_secure(self,conn, mask):
        context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
        context.load_cert_chain(self.ca_cert_file, self.key_file)
        conn.send(self.nats_proto_info_reply(str(conn.getsockname()[0]),
                                             str(conn.getsockname()[1]),
                                             str(conn.getpeername()[0]),
                                             str(conn.getpeername()[1])))
        ssock = context.wrap_socket(conn, server_side=True)
        sconn, saddr = ssock.accept()
        sconn.setblocking(self.blocking)
        self.sel.register(sconn, selectors.EVENT_READ, self.read)

    def read(self,conn, mask):
        data = conn.recv(self.buffer_size)  # Should be ready
        if data:
            #print('echoing', repr(data), 'to', conn)
            BUFFER_BYTES.extend(data)
            #conn.send(data)  # Hope it won't block
        elif "PING" in [x.strip() for x in str(data).split("\n") if x != ""]:
            conn.send("PONG\r\n")
        else:
        #if conn.fileno() < 0:
            print('closing', conn)
            self.sel.unregister(conn)
            conn.close()

    def nats_proto_info_reply(self,server_ip:str,port: str, client_ip:str, client_id: str):
        ip_port = f"{server_ip}:{port}"
        server_name = str(socket.gethostname())
        info_reply = f""""server_id":"1","server_name":"{server_name}","version":"2.6.6","proto":1,"git_commit":"878afad","go":"go1.16.10","host":"0.0.0.0","port":{port},"headers":true,"tls_required":true,"max_payload":1048576,"jetstream":false,"client_id":{str(client_id)},"client_ip":"{client_ip}","cluster":"monitoring","connect_urls":["{ip_port}"]"""
        info_reply = "INFO {" + info_reply + "}\r\n"
        return info_reply.encode()

class NatsServer2():
    def __init__(self,
                 host:str,
                 port:int,
                 buffer_size: int,
                 secure:bool=False,
                 cert_file:str="",
                 key_file:str="",
                 ca_file:str=""
                 ):
        self.sel = selectors.DefaultSelector()
        self.host = host
        self.port = port
        self.buffer_size = buffer_size
        self.secure = secure
        self.cert_file = cert_file
        self.key_file = key_file
        self.ca_file = ca_file

    def start(self):
        print("Start tcp server 2")
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            sock.bind((self.host,self.port))
            sock.listen()
            while True:
                s, address = sock.accept()
                print('accepted', s, 'from', address)
                if self.secure:
                    s.send(self.nats_proto_info_reply(str(s.getsockname()[0]),
                                                         str(s.getsockname()[1]),
                                                         str(s.getpeername()[0]),
                                                         str(s.getpeername()[1])))
                    context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
                    context.load_cert_chain(self.cert_file, self.key_file)
                    #context.check_hostname = False
                    #context.verify_mode = ssl.CERT_NONE
                    context.load_verify_locations(self.ca_file)
                    s = context.wrap_socket(s, server_side=True)
                    sconn, saddr = s.accept()

                # loop serving the new client
                while True:
                    receivedData = s.recv(self.buffer_size)
                    if not receivedData:
                        break
                    elif "PING" in [x.strip() for x in str(receivedData).split("\n") if x != ""]:
                        s.send(b"PONG\r\n")
                    # Echo back the same data you just received
                    print(s,receivedData)
                s.close()
                print("Disconnected", s, 'from', address)
        except:
            sys.excepthook(*sys.exc_info())

    def nats_proto_info_reply(self,server_ip:str,port: str, client_ip:str, client_id: str):
        ip_port = f"{server_ip}:{port}"
        server_name = str(socket.gethostname())
        info_reply = f""""server_id":"1","server_name":"{server_name}","version":"2.6.6","proto":1,"git_commit":"878afad","go":"go1.16.10","host":"0.0.0.0","port":{port},"headers":true,"tls_required":true,"max_payload":1048576,"jetstream":false,"client_id":{str(client_id)},"client_ip":"{client_ip}","cluster":"monitoring","connect_urls":["{ip_port}"]"""
        info_reply = "INFO {" + info_reply + "}\r\n"
        return info_reply.encode()


def read_from_server(some:int ,queue: queue.Queue) -> None:
    print("start read from server and put to queue")
    separator = b"\n"
    while True:
        start_time = time.time()
        new_line_last_index = BUFFER_BYTES.rfind(separator)
        if new_line_last_index > 0:
            raw_influx_proto_strings = BUFFER_BYTES[:new_line_last_index]
            del BUFFER_BYTES[:new_line_last_index]
            for element in raw_influx_proto_strings.split(separator):
                queue.put(element)
        running_time = time.time() - start_time
        if running_time < 1:
            time.sleep(1 - running_time)

def read_from_queue(some:int , queue: queue.Queue):
    print("start read from queue")
    while True:
        print(queue.get())
        time.sleep(1)

def sendKillSignal(etype, value, tb):
    print('KILL ALL')
    traceback.print_exception(etype, value, tb)
    os.kill(os.getpid(),9)

def main() -> None:
    sys.excepthook = sendKillSignal
    get_configuration = Configuration()
    configuration = get_configuration.read()

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                        level=logging.INFO)
    logger = logging.getLogger(__name__)

    if configuration['log_level'] == 'DEBUG':
        logger.setLevel(logging.DEBUG)
    elif configuration['log_level'] == 'INFO':
        logger.setLevel(logging.INFO)
    elif configuration['log_level'] == 'ERROR':
        logger.setLevel(logging.ERROR)
    elif configuration['log_level'] == 'CRITICAL':
        logger.setLevel(logging.CRITICAL)
    elif configuration['log_level'] == 'WARNING':
        logger.setLevel(logging.WARNING)
    else:
        logger.setLevel(logging.NOTSET)

    main_queue = queue.Queue()

    #print(configuration)
    servers = []
    for server_cfg in configuration['server']['nats']:
        print(server_cfg)
        host = server_cfg['listen'].split(':')[0]
        port = server_cfg['listen'].split(':')[1]
        if 'key' in server_cfg.keys() and 'ca' in server_cfg.keys() and 'cert' in server_cfg.keys():
            key_file = server_cfg['key']
            cert_file = server_cfg['cert']
            ca_file = server_cfg['ca']
            os.environ["REQUESTS_CA_BUNDLE"] = server_cfg['ca']
            os.environ["SSL_CERT_FILE"] = server_cfg['ca']
            server = NatsServer2(host=host,
                                port=int(port),
                                cert_file=cert_file,
                                key_file=key_file,
                                ca_file=ca_file,
                                secure=True,
                                buffer_size=1000)
            server_thread = Thread(target=server.start)
            server_thread.start()
        #else:
        #    server = NatsServer(host=host,
        #                        port=int(port),
        #                        buffer_size=1000)
        #    server_thread = Thread(target=server.start)
        #    server_thread.start()


    t1 = Thread(target=read_from_queue, args=(1,main_queue))
    t1.start()

    t2 = Thread(target=read_from_server, args=(1,main_queue))
    t2.start()

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()
