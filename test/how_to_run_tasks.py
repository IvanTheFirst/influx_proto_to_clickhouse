import asyncio
from datetime import datetime
import time
import concurrent.futures
import signal

GLOBAL_LIST = list()

async def append():
    print("append is running")
    global GLOBAL_LIST
    while True:
        length = len(GLOBAL_LIST)
        GLOBAL_LIST.append(len(GLOBAL_LIST))
        timestamp = str(datetime.now().strftime("%Y.%m.%d %H:%M:%S"))
        print(f'{timestamp}. Append add: {length}')
        await asyncio.sleep(0.6)

async def pop_async():
    print("pop is running")
    global GLOBAL_LIST
    while True:
        await asyncio.sleep(1)
        if len(GLOBAL_LIST) > 0:
            timestamp = str(datetime.now().strftime("%Y.%m.%d %H:%M:%S"))
            print(f"{timestamp}. Remove top element :{GLOBAL_LIST.pop(0)}. Length: {len(GLOBAL_LIST)}")

def pop():
    print("pop is running")
    global GLOBAL_LIST
    while True:
        time.sleep(1)
        if len(GLOBAL_LIST) > 0:
            timestamp = str(datetime.now().strftime("%Y.%m.%d %H:%M:%S"))
            print(f"{timestamp}. Remove top element :{GLOBAL_LIST.pop(0)}. Length: {len(GLOBAL_LIST)}")

async def main():
    ################################################
    # YES
    append_task = asyncio.create_task(append())
    pop_async_task = asyncio.create_task(pop_async())
    await append_task
    await pop_async_task
    ################################################
    # NO
    #await asyncio.gather(
    #    asyncio.to_thread(await pop_async()),
    #    asyncio.create_task(append())
    #)
    #################################################
    # NO
    #thread_pool = concurrent.futures.ThreadPoolExecutor(2)
    #@loop = asyncio.get_running_loop()
    #await asyncio.gather(
    #    await loop.run_in_executor(thread_pool, pop()),
    #    append()
    #)
    ##################################################
    #await loop.run_in_executor(thread_pool, pop())
    #await asyncio.create_task(append())

    #loop = asyncio.get_running_loop()
    #with concurrent.futures.ThreadPoolExecutor() as pool:
    #    result = await loop.run_in_executor(
    #        pool, pop)
    #await asyncio.to_thread(pop())
    #await asyncio.gather(
    #    asyncio.to_thread(pop())
    #)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    asyncio.run(main())