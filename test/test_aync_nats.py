import asyncio
import ssl
from nats.aio.client import Client as NATS
from nats.errors import TimeoutError

async def main():
    nc = NATS()
    await nc.connect(["tls://v00zonmonq02:4222"])

    # Create JetStream context.
    js = nc.jetstream()

    sub = await js.subscribe(["TELEGRAF"])
    msg = await sub.next_msg()
    await msg.ack()

    # Create ordered consumer with flow control and heartbeats
    # that auto resumes on failures.
    osub = await js.subscribe("PER_SECOND_SYSTEM", ordered_consumer=True)
    data = bytearray()

    while True:
        try:
            msg = await osub.next_msg()
            data.extend(msg.data)
        except TimeoutError:
            break
    print("All data in stream:", len(data))

    await nc.close()

if __name__ == '__main__':
    asyncio.run(main())