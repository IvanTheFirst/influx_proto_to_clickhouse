import sys
from threading import Thread,RLock
import logging
import signal
import time
from nats_simple import nats_simple
from arguments import Configuration
from influx_parser_writer import ParserLoader
from line_protocol_parser import parse_line as parse_influx_line
import queue
from pympler import asizeof
import asyncio
import nats

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

PARSER_QUEUES = {}
PARSER_THREADS = {}
WRITER_QUEUE = queue.Queue()
lock = RLock()

class Queue:
    def __init__(self):
        self._loop = asyncio.get_running_loop()
        self._queue = asyncio.Queue()

    def sync_put_nowait(self, item):
        self._loop.call_soon(self._queue.put_nowait, item)

    def sync_put(self, item):
        asyncio.run_coroutine_threadsafe(self._queue.put(item), self._loop).result()

    def sync_get(self):
        return asyncio.run_coroutine_threadsafe(self._queue.get(), self._loop).result()

    def async_put_nowait(self, item):
        self._queue.put_nowait(item)

    async def async_put(self, item):
        await self._queue.put(item)

    async def async_get(self):
        return await self._queue.get()

def writer(clickhouse,queue_to_write,writer_thread_id):
    logger.debug(f'writer thread started number {writer_thread_id}')
    while True:
        start_time = time.time()
        if queue_to_write.qsize() > 0:
            clickhouse.writer(read_all_from_queue(WRITER_QUEUE))

        #logger.debug(f"writer id {writer_thread_id} clickhouse obj for write: {asizeof.asizeof(clickhouse)}")
        running_time = time.time() - start_time
        if running_time < 1:
            time.sleep(1 - running_time)

def parser(clickhouse,queue_to_read,queue_to_write,table_name):
    logger.debug(f"{table_name} - parsing thread started")
    while True:
        start_time = time.time()
        #logger.debug(f"{table_name} queue size {asizeof.asizeof(queue_to_read)}, clickhouse obj for parse: {asizeof.asizeof(clickhouse)}")
        elements = read_all_from_queue(queue_to_read)
        if len(elements) > 0:
            queue_to_write.put(clickhouse.for_json_to_sql(elements, table_name))
        running_time = time.time() - start_time
        if running_time < 1:
            time.sleep(1 - running_time)

def read_all_from_queue(queue):
    output = list()
    while not queue.empty():
        output.append(queue.get())
        queue.task_done()
    return output

async def from_msg_to_json(msg):
    try:
        msg = msg.payload.decode()
    except Exception as e:
        pass
    parsed_json = {}
    try:
        parsed_json = parse_influx_line(msg)
    except Exception as e:
        logger.debug(f"Converting from influx line proto to dict: {str(e)} - {msg}")
        return None
    if (parsed_json['measurement'] not in PARSER_QUEUES.keys()):
        with lock:
            PARSER_QUEUES[parsed_json['measurement'] ] = queue.Queue()
    new_tags = {f"tag_{k}": v for k, v in parsed_json['tags'].items()}
    PARSER_QUEUES[parsed_json['measurement']].put_nowait({**new_tags, **parsed_json['fields'], **{'temp_timestamp': parsed_json['time']}})

async def read_from_one_nats_server(nats_server: str,
                                    subject:str ):
    nc = await nats.connect(nats_server)
    js = nc.jetstream()
    osub = await js.subscribe(subject, ordered_consumer=True)
    while True:
        try:
            msg = await osub.next_msg()
            await from_msg_to_json(msg)
            #parser_queue.async_put_nowait(await osub.next_msg())
            # data.extend(msg.data)
        except Exception as e:
            logger.info(f'Some thing goes wrong with server: {nats_server}, error: {str(e)}')

def read_from_nats(nats_servers: list,
                   subject:str) -> None:
    loop = asyncio.new_event_loop()
    nats_tasks = []
    for nats_server in nats_servers:
        task = asyncio.create_task(read_from_one_nats_server(nats_server, subject))
        nats_tasks.append(task)
    loop.run_forever()

def main():
    get_configuration = Configuration()
    """(LOGLEVEL,
     readbufftime,dbname,dbsecure,dbhost,dbuser,dbpass,ttl,table_engine,chcluster,
     nats_servers,nats_scheme,path_to_nats_bin,client_name,server_stream,
     client_subject,writers_count,mem_stat) = get_configuration.cmd_arguments()
     """
    arg_tuple = get_configuration.cmd_arguments()

    (LOGLEVEL,
     readfromnats, dbname, dbsecure, dbhost, dbuser, dbpass, ttl, table_engine, chcluster,
     nats_servers, nats_scheme, path_to_nats_bin, client_name, server_stream,
     client_subject, writers_count, mem_stat, stdin_input) = arg_tuple

    if LOGLEVEL == 'DEBUG':
        logger.setLevel(logging.DEBUG)
    elif LOGLEVEL == 'INFO':
        logger.setLevel(logging.INFO)
    elif LOGLEVEL == 'ERROR':
        logger.setLevel(logging.ERROR)
    elif LOGLEVEL == 'CRITICAL':
        logger.setLevel(logging.CRITICAL)
    elif LOGLEVEL == 'WARNING':
        logger.setLevel(logging.WARNING)
    else:
        logger.setLevel(logging.NOTSET)

    clickhouse_to_parse = ParserLoader(LOGLEVEL,dbname,dbsecure,dbhost,dbuser,dbpass,ttl,table_engine,chcluster)

    clickhouse_to_parse.connect_to_db()
    clickhouse_to_parse.get_db_scheme()

    writer_threads_list = []
    writer_clickhouse_connections = []

    # запускаем треды для записи
    for writer_thread_id in range(writers_count):
        clickhouse_to_write = ParserLoader(LOGLEVEL, dbname, dbsecure, dbhost, dbuser, dbpass, ttl, table_engine,
                                           chcluster)
        clickhouse_to_write.connect_to_db()
        writer_clickhouse_connections.append(clickhouse_to_write)
        writer_thread = Thread(target=writer,args=(clickhouse_to_write,WRITER_QUEUE,writer_thread_id,))
        writer_threads_list.append(writer_thread)
        writer_thread.start()

    # блок получения данных
    # запуск async NATS клиентов
    th_input = Thread(target=read_from_nats, args=(nats_servers, client_subject), daemon=True)
    th_input.start()

    #основная часть с парсингом и записью
    while True:
        start_time = time.time()

        try:
            with lock:
                for table_name in PARSER_QUEUES.keys():
                    if table_name not in PARSER_THREADS.keys():
                        PARSER_THREADS[table_name] = Thread(target=parser, args=(clickhouse_to_parse,PARSER_QUEUES[table_name],WRITER_QUEUE,table_name,))
                        PARSER_THREADS[table_name].start()

        except Exception as e:
            logger.info(f'Error with something in starting parser threads {str(e)}')

        running_time = time.time() - start_time
        if running_time < 1:
            time.sleep(1 - running_time)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()
