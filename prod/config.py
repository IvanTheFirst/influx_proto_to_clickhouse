from argparse import ArgumentParser
import sys
import toml
import re

SAMPLE_CONFIG = """
## DEBUG, INFO, WARNING, ERROR, CRITICAL, default NOTSET
log_level = "NOTSET"

[[server.udp]]
    listen = "0.0.0.0:9100"
#   # data_format maybe influx_proto only for now
    data_format = "influx_proto"
    database = 'monitoring'

[clickhouse]
## clickhouse connection scheme secure True/False
secure = true
## clickhouse host and port
host = "127.0.0.1:9443"
login = "some login"
password = "some password"
## clickhouse cluster to create tables
cluster = "monitoring"
## total number of writers to clickhouse
writers_count = 1
ttl = 2
table_engine = 'ReplicatedMergeTree'
"""

# Server Types
SERVER_UDP = 'udp'
SUPPORTED_SERVERS = [SERVER_UDP]

# data formats
DATA_INFLUX_PROTO = 'influx_proto'
SUPPORTED_DATA_FORMATS = [DATA_INFLUX_PROTO]
DEFAULT_CONFIG_PATH = 'metrics_to_clickhouse.toml'

class Configuration:
    def __init__(self):
        #self.dict_of_buffers = dict_of_buffers
        pass

    def create_config(self,create_config_path):
        f = open(create_config_path,mode="w")
        f.write(SAMPLE_CONFIG)
        f.close()

    def read(self):
        parser = ArgumentParser(prog='PROG')
        parser.add_argument("-c","--config", dest="config",
                            help="path to configuration file",
                            required=False,
                            default=DEFAULT_CONFIG_PATH)
        parser.add_argument("--create_config", dest="create_config",
                            help=f"Create default config {DEFAULT_CONFIG_PATH}",
                            default='-1',
                            required=False)
        args = parser.parse_args()
        configuration_file = vars(args)['config']
        create_config_path = vars(args)['create_config']
        if create_config_path != '-1':
            self.create_config(create_config_path)
            sys.exit()
        output = toml.load(configuration_file)
        ######
        ## test for config settings
        if SERVER_UDP in output['server'].keys():
            for udp_server_record in output['server'][SERVER_UDP]:
                if 'data_format' not in udp_server_record.keys():
                    sys.exit(f"There is no data_format in udp server record")
                else:
                    if udp_server_record['data_format'] not in SUPPORTED_DATA_FORMATS:
                        sys.exit(f"unsupported data_format in udp server record: {udp_server_record}")
                if 'database' not in udp_server_record.keys():
                    sys.exit(f"There is no database in udp server record")
        return output

