from datetime import datetime
import pandas as pd
import numpy as np
from io import StringIO
import logging
from clickhouse_driver import connect

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


class ParserLoader():
    def __init__(self,
                 loglevel,
                 dbsecure,
                 dbhost,
                 dbuser,
                 dbpass,
                 ttl,
                 table_engine,
                 chcluster
                 ):
        logger.setLevel(loglevel)
        #self.dbname = dbname
        self.dbsecure = dbsecure
        self.dbhost = dbhost
        self.dbuser = dbuser
        self.dbpass = dbpass
        self.ttl = ttl
        self.table_engine = table_engine
        self.chcluster = chcluster
        #self.db_scheme = {}
        self.click_connection = None
        #self.inserts = [] # [list of schema inserts, list of csv insert]

    def connect_to_db(self):
        logger.info(f'Connected to DB {self.dbhost}')
        [host, port] = self.dbhost.split(':')
        self.click_connection = connect(user=self.dbuser, password=self.dbpass,
                       host=host, port=port, secure=self.dbsecure)
        #return conn

    def get_db_scheme(self,database_name):
        #logger.info('Get schema')
        cursor = self.click_connection.cursor()
        query = f'SHOW TABLES FROM `{database_name}`'
        cursor.execute(query)
        tables = []
        for table in cursor.fetchall():
            tables.append(table[0])
        dict_tables = {}
        for table in tables:
            if table not in dict_tables.keys():
                dict_tables[table] = []
            query = f'describe table `{database_name}`.`{table}`'
            cursor.execute(query)
            dict_tables[table] = []
            for row in cursor.fetchall():
                dict_tables[table].append(row[0])
        db_scheme = dict_tables.copy()
        cursor.close()
        return db_scheme

    def from_json_to_sql(self,elements,database_name,table_name,db_scheme):
        #logger.debug(f'{table_name} - {elements[0:100]}')
        df = pd.DataFrame(elements)
        df['timestamp'] = df['temp_timestamp'].apply(lambda x: datetime.fromtimestamp(x / 1e9))
        df.drop('temp_timestamp', axis=1, inplace=True)
        types = {}
        for col_name in df:
            if df.dtypes[col_name] == object:
                types[col_name] = 'LowCardinality(String) CODEC(ZSTD)'
            elif df.dtypes[col_name] == bool:
                df[col_name] = df[col_name].replace({True: 1, False: 0})
                types[col_name] = 'UInt8'
            elif df.dtypes[col_name] == np.dtype('datetime64[ns]'):
                types[col_name] = 'DateTime64 CODEC(DoubleDelta,ZSTD)'
            elif df.dtypes[col_name] == np.half:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            # elif df.dtypes[col_name] == np.floating:
            #    types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.float_:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.float64:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.double:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.single:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.float16:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.float32:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.int0:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.int16:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.int32:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.int64:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.int8:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.int_:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.intc:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.integer:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.integer:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.intp:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.signedinteger:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.short:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.longlong:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.uint:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.uint0:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.uint16:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.uint32:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.uint64:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.uint8:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.uintc:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.uintp:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.unsignedinteger:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.ushort:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            elif df.dtypes[col_name] == np.ulonglong:
                types[col_name] = 'Float64 CODEC(Gorilla,ZSTD)'
            else:
                types[col_name] = 'String'
            # adding ttl
            # types[col_name] = f"{types[col_name]} TTL toDateTime(timestamp) + INTERVAL {ttl} DAY"
        # Creating inserts for creation tables and rows
        schema_inserts = []
        #db_scheme = self.get_db_scheme(database_name)
        if table_name not in db_scheme.keys():
            create_table = pd.io.sql.get_schema(df, table_name, dtype=types) + str("\n".join([
                f"ENGINE = {self.table_engine} ORDER BY timestamp",
                f"TTL toDateTime(timestamp) + INTERVAL {self.ttl} DAY DELETE"]))
            create_table = create_table.replace('CREATE TABLE ', f'CREATE TABLE IF NOT EXISTS {database_name}.')
            create_table = create_table[:create_table.find('(')] + f' on cluster {self.chcluster} (' + create_table[
                                                                                                       create_table.find(
                                                                                                           '(') + 1:]
            logger.debug(create_table)
            schema_inserts.append(create_table)
            db_scheme[table_name] = list(types.keys())
        else:
            columns_to_add = list(set(df.columns) - set(db_scheme[table_name]))
            if len(columns_to_add) > 0:
                for colunm in columns_to_add:
                    db_scheme[table_name].append(colunm)
                    schema_inserts.append(
                        f"""ALTER TABLE `{database_name}`.`{table_name}` ADD COLUMN IF NOT EXISTS `{colunm}` {types[colunm]} """)

        output_csv = StringIO()
        df.to_csv(output_csv, index=False, quoting=1)
        output_csv = f"INSERT INTO `{database_name}`.`{table_name}` FORMAT CSVWithNames \n" + output_csv.getvalue()
        #logger.debug(f'{table_name} - {output_csv[0:100]}')
        return [schema_inserts, output_csv, db_scheme]

    def writer(self,inserts):
        cursor = self.click_connection.cursor()
        for temp_insert in inserts:
            schema_inserts = temp_insert[0]
            output_csv = temp_insert[1]
            if len(schema_inserts) > 0:
                logger.info(f"Creating tables or columns {schema_inserts}")
                for insert in schema_inserts:
                    try:
                        logger.debug(insert)
                        cursor.execute(insert)
                    except Exception as e:
                        logger.info(insert)
                        logger.info(str(e))
            try:
                logger.debug(f"Trying to write {str(len(output_csv.splitlines()) - 1)} to db")
                cursor.execute(output_csv)
            except Exception as e:
                logger.info(str(e))
                logger.info(output_csv[:4000])
        cursor.close()

