import sys
from threading import Thread,Lock,RLock
import logging
import signal
import time
from config import Configuration
from clickhouse_parser_writer import ParserLoader
from line_protocol_parser import parse_line as parse_influx_line
import queue
import socket
import array

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

# Server Types
SERVER_UDP = 'udp'

# data formats
DATA_INFLUX_PROTO = 'influx_proto'

# each buffer for each data_format
BUFFER_DATA_INFLUX_PROTO = bytearray()
PARSER_QUEUE = queue.Queue()
WRITER_QUEUE = queue.Queue()
CLICKHOUSE_DB_SCHEME = {}
threadLock_for_variable = Lock()
threadLock_for_q = RLock()

def clickhouse_writer(clickhouse: ParserLoader,queue_to_write,writer_thread_id):
    logger.debug(f'writer thread started number {writer_thread_id}')
    while True:
        start_time = time.time()
        if queue_to_write.qsize() > 0:
            clickhouse.writer(read_all_from_queue(WRITER_QUEUE))

        #logger.debug(f"writer id {writer_thread_id} clickhouse obj for write: {asizeof.asizeof(clickhouse)}")
        running_time = time.time() - start_time
        if running_time < 1:
            time.sleep(1 - running_time)

def read_all_from_queue(queue):
    output = list()
    while not queue.empty():
        output.append(queue.get())
        queue.task_done()
    return output

def influx_proto_from_msg_to_json(msg):
    try:
        msg = msg.payload.decode()
    except Exception as e:
        pass
    try:
        msg = msg.decode()
    except Exception as e:
        pass
    parsed_json = {}
    try:
        parsed_json = parse_influx_line(msg)
    except Exception as e:
        logger.debug(f"Converting from influx line proto to dict: {str(e)} - {msg}")
        return None
    return parsed_json
####
# SERVER
def server_udp(logger,host_port,data_format,buffer_size=0):
    host = host_port.split(':')[0]
    port = int(host_port.split(':')[1])
    # Create a datagram socket
    UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    #get os buffer size
    if buffer_size <= 0:
        bufferSize = int(UDPServerSocket.getsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF))
    else:
        bufferSize = buffer_size
    # Bind to address and ip
    UDPServerSocket.bind((host,port))
    logger.info(f"UDP server up and listening. {host}:{port} ")
    # Listen for incoming datagrams
    if data_format == DATA_INFLUX_PROTO:
        while (True):
            bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
            BUFFER_DATA_INFLUX_PROTO.extend(bytesAddressPair[0])

####
# from SERVER to Queue for threads
def from_buffer_bytes_to_buffer_queue(logger,server_type,database_name,data_format):
    separator = b""
    if server_type == SERVER_UDP and data_format == DATA_INFLUX_PROTO:
        separator = b"\n"
        while True:
            start_time = time.time()
            new_line_last_index = BUFFER_DATA_INFLUX_PROTO.rfind(separator)
            if new_line_last_index > 0:
                raw_influx_proto_strings = BUFFER_DATA_INFLUX_PROTO[:new_line_last_index]
                del BUFFER_DATA_INFLUX_PROTO[:new_line_last_index]
                PARSER_QUEUE.put((data_format,database_name,separator,raw_influx_proto_strings))
            running_time = time.time() - start_time
            if running_time < 1:
                time.sleep(1 - running_time)

def clickhouse_parser(database_connection: ParserLoader):
    bytes_from_buf = read_all_from_queue(PARSER_QUEUE)
    for byte_from_buf in bytes_from_buf:
        data_format,database_name,separator,raw_data_block = byte_from_buf
        if database_name not in CLICKHOUSE_DB_SCHEME.keys():
            with threadLock_for_variable:
                CLICKHOUSE_DB_SCHEME[database_name]=database_connection.get_db_scheme(database_name)
        result = {}
        for element in raw_data_block.split(separator):
            parsed_json = influx_proto_from_msg_to_json(element)
            if not parsed_json: continue
            if parsed_json['measurement'] not in result.keys():
                result[parsed_json['measurement']] = []
            new_tags = {f"tag_{k}": v for k, v in parsed_json['tags'].items()}
            result[parsed_json['measurement']].append({**new_tags, **parsed_json['fields'], **{'temp_timestamp': parsed_json['time']}})
        for table_name in result.keys():
            [schema_inserts, output_csv, db_scheme] = database_connection.from_json_to_sql(result[table_name],
                                                                                         database_name,
                                                                                         table_name,
                                                                                         CLICKHOUSE_DB_SCHEME[database_name])
            WRITER_QUEUE.put([schema_inserts, output_csv])
            if not set(CLICKHOUSE_DB_SCHEME[database_name][table_name]) == set(db_scheme[table_name]):
                with threadLock_for_variable:
                    CLICKHOUSE_DB_SCHEME[database_name][table_name] = db_scheme

def main():
    get_configuration = Configuration()
    configuration = get_configuration.read()

    if configuration['log_level'] == 'DEBUG':
        logger.setLevel(logging.DEBUG)
    elif configuration['log_level'] == 'INFO':
        logger.setLevel(logging.INFO)
    elif configuration['log_level'] == 'ERROR':
        logger.setLevel(logging.ERROR)
    elif configuration['log_level'] == 'CRITICAL':
        logger.setLevel(logging.CRITICAL)
    elif configuration['log_level'] == 'WARNING':
        logger.setLevel(logging.WARNING)
    else:
        logger.setLevel(logging.NOTSET)

    # запускаем серверы и перекладыватели из буфера в очередь
    servers_threads = {}
    raw_data_to_queue_threads = {}
    clickhouse_for_parser = {}
    for server in configuration['server']:
        # старт сервера, если такой ещё не запущен\
        if SERVER_UDP == server:
            if SERVER_UDP not in servers_threads.keys():
                servers_threads[SERVER_UDP] = {}
            # старт сервера, если такой ещё не запущен
            for each_udp_server in configuration['server'][SERVER_UDP]:
                if each_udp_server['listen'] not in servers_threads.keys():
                    server_thread = Thread(target=server_udp, args=(logger,
                                                            each_udp_server['listen'],
                                                            each_udp_server['data_format']))
                    server_thread.start()
                    servers_threads[SERVER_UDP]['listen'] = {'server': server_thread}
                # запуск перекладывателя для data_format, если он ещё не запущен
                if each_udp_server['data_format'] not in raw_data_to_queue_threads.keys():
                    raw_data_to_queue_thread = Thread(target=from_buffer_bytes_to_buffer_queue,
                                                  args=(logger,SERVER_UDP,each_udp_server['database'],each_udp_server['data_format']))
                    raw_data_to_queue_thread.start()
                    raw_data_to_queue_threads[each_udp_server['data_format']] = raw_data_to_queue_thread


    # запускаем потоки для записи в кликхаус
    writer_threads_list = []
    writer_clickhouse_connections = []

    for writer_thread_id in range(configuration['clickhouse']['writers_count']):
        clickhouse_to_write = ParserLoader(configuration['log_level'],
                                       configuration['clickhouse']['secure'],
                                       configuration['clickhouse']['host'],
                                       configuration['clickhouse']['login'],
                                       configuration['clickhouse']['password'],
                                       configuration['clickhouse']['ttl'],
                                       configuration['clickhouse']['table_engine'],
                                       configuration['clickhouse']['cluster'])
        clickhouse_to_write.connect_to_db()
        writer_clickhouse_connections.append(clickhouse_to_write)
        writer_thread = Thread(target=clickhouse_writer,args=(clickhouse_to_write,WRITER_QUEUE,writer_thread_id,))
        writer_threads_list.append(writer_thread)
        writer_thread.start()

    ###
    # clickhouse connection for database scheme
    clickhouse_scheme = ParserLoader(configuration['log_level'],
                                       configuration['clickhouse']['secure'],
                                       configuration['clickhouse']['host'],
                                       configuration['clickhouse']['login'],
                                       configuration['clickhouse']['password'],
                                       configuration['clickhouse']['ttl'],
                                       configuration['clickhouse']['table_engine'],
                                       configuration['clickhouse']['cluster'])
    clickhouse_scheme.connect_to_db()
    while True:
        start_time = time.time()
        if not PARSER_QUEUE.empty():
            parser_to_sql_thread = Thread(target=clickhouse_parser,args=(clickhouse_scheme,))
            parser_to_sql_thread.start()
        running_time = time.time() - start_time
        if running_time < 1:
            time.sleep(1 - running_time)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()
