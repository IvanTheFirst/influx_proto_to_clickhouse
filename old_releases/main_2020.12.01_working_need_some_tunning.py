# дальше вот так (созадаим loop в треде(нити))
# https://gist.github.com/dmfigol/3e7d5b84a16d076df02baa9f53271058
#
# udo server from
# https://pythontic.com/modules/socket/udp-client-server-example
#
##############################################################
#
# Запукает два треда, синхронный UDP сервер в основном треде, в другом запускат asyncio. Там где asyncio идёт сохранение данных
# из UDP сервера. В качестве переменных для буфера между UDP сервером и сохранением данных используется bytearray,
# потому что потребляет мало памяти, но при этом почему-то больше чем 1711271416 байт (но я думаю 2ГБ по факту), поместить в
# переменную не получается.
# Есть 2 нюанса:
# 1. Буфер отчищается командой del[:индекс], очень круто и быстро
# 2. Из-за ограничений на размер переменной bytearray, пришлось создавать глобальные переменные с числовым суффиком,
#    например STORAGE_0, STORAGE_1 и так далее. Команда вида globals()['имя переменной'] отлично для этого подходит.
# 3. Новые переменные STORAGE создаются, когда старая + новые данные из буфера, становятся больше 1 ГБ -
#    задётся через SIZE_OF_ONE_STORAGE_BYTES
# 4. переменные сжимаются через zlib, гигабайт за 20 секунд сжимался
#
# Результат:
# 1. читайет по 500к-600к строк за 10 секунд без проблем
# 2. Cжимает хорошо, 30-50 МБ из 1 ГБ, час сжатых метрик ~ 2ГБ в ОЗУ, это примерно 72 814 960 строк
#
# что с этим делать дальше пока не ясно

from threading import Thread,current_thread
import logging
from argparse import ArgumentParser
from datetime import datetime
from pympler import asizeof
import signal
import socket
import time
from line_protocol_parser import parse_line as parse_influx_line
from multiprocessing import Process
import pandas as pd
import numpy as np
from io import StringIO

#import sqlalchemy
#from sqlalchemy_clickhouse.base import VARCHAR,TIMESTAMP
#from sqlalchemy.dialects import registry
#import sqlalchemy_clickhouse as sqlalchemy_clickhouse

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

BUFFER = bytearray()
LINES_COUNT = 0
FIRST_RUN = True
DATABASE_SCHEMA = dict()

###############################################################
# ACTIVE FUNCTIONS

def statistics():
    header = " - ".join(["elements", "buffer pympler size"])
    logger.info(header)
    while True:
        time.sleep(10)
        line = " - ".join([str(LINES_COUNT),str(asizeof.asizeof(BUFFER))])
        logger.info(line)

###########################################################
# UDP server

def udp_server(IP,PORT,buffer_size):
    # Create a datagram socket
    UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    #get os buffer size
    if buffer_size <= 0:
        bufferSize = int(UDPServerSocket.getsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF))
    else:
        bufferSize = buffer_size
    # Bind to address and ip
    UDPServerSocket.bind((IP,PORT))
    logger.info(f"UDP server up and listening. {IP}:{PORT} ")
    # Listen for incoming datagrams
    while (True):
        bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
        BUFFER.extend(bytesAddressPair[0])

def parser(dbname,dbhost,dbuser,dbpass,readbufftime) -> None:
    global LINES_COUNT
    logger.info("Parser started")
    while True:
        start_time = time.time()
        new_line_last_index = BUFFER.rfind(b"\n")
        if new_line_last_index > 0:
            element = BUFFER[:new_line_last_index]
            del BUFFER[:new_line_last_index]
            elements = element.splitlines()
            current_elem_lines = len(elements)
            LINES_COUNT = LINES_COUNT + current_elem_lines

            p = Process(target=parser_process, args=(elements,dbname,dbhost,dbuser,dbpass,))
            p.start()
            #p.join()

        running_time = time.time() - start_time
        if running_time < readbufftime:
            time.sleep(readbufftime - running_time)

def parser_process(elements,dbname,dbhost,dbuser,dbpass) -> None:

    output = dict()
    #start_time = time.time()
    for line in elements:
        temp = {}
        try:
            temp = parse_influx_line(line.decode())
        except Exception as e:
            # logger.error(f"Converting from UDP line proto to dict: {str(traceback.format_exc())} - {line}")
            continue
        if temp['measurement'] not in output.keys():
            output[temp['measurement']] = list()
        output[temp['measurement']].append({**temp['tags'],**temp['fields'],**{'temp_timestamp':temp['time']}})
        #temp['time'] = (datetime.fromtimestamp(temp['time'] / 1e9)).strftime('%Y-%m-%d %H:%M:%S.%f')[:-4]
        #parsed.append(from_influx_line_proto_to_inserts_and_types(dbname,temp))
    #list_of_dfs = []
    insert_threads = []
    for table in output.keys():
        df = pd.DataFrame(output[table])
        df['timestamp'] = df['temp_timestamp'].apply(lambda x: datetime.fromtimestamp(x / 1e9))
        df.drop('temp_timestamp', axis=1, inplace=True)
        types = {}
        for col_name in df:
            if df.dtypes[col_name] == np.object:
                types[col_name] = 'String'
            elif df.dtypes[col_name] == np.float64:
                types[col_name] = 'Float64'
            elif df.dtypes[col_name] == np.bool:
                df[col_name] = df[col_name].replace({True: 1, False: 0})
                types[col_name] = 'UInt8'
            elif df.dtypes[col_name] == np.dtype('datetime64[ns]'):
                types[col_name] = 'DateTime64'
            else:
                types[col_name] = 'String'
        # Creating inserts for creation tables and rows
        schema_inserts = []
        if table not in DATABASE_SCHEMA.keys():
            create_table = pd.io.sql.get_schema(df,table, dtype=types) + str(
                "ENGINE = MergeTree ORDER BY timestamp PARTITION BY toYYYYMM(timestamp)")
            create_table.replace('CREATE TABLE ', f'CREATE TABLE IF NOT EXISTS `{dbname}.`')
            schema_inserts.append(create_table)
            DATABASE_SCHEMA[table] = list(types.keys())
        else:
            columns_to_add = list(set(df.columns) - set(DATABASE_SCHEMA[table]))
            if len(columns_to_add)>0:
                for colunm in columns_to_add:
                    schema_inserts.append(f"""ALTER TABLE `{dbname}`.`{table}` ADD COLUMN IF NOT EXISTS `{colunm}` {types[colunm]} """)

        output_csv = StringIO()
        df.to_csv(output_csv, index=False, quoting=1)
        output_csv = f"INSERT INTO `{dbname}`.`{table}` FORMAT CSVWithNames \n" + output_csv.getvalue()

        t = Thread(target=insert, args=(output_csv,dbuser,dbpass,dbhost,dbname,schema_inserts),
                   daemon=True)
        insert_threads.append(t)

    for t in insert_threads:
        t.start()

    for t in insert_threads:
        t.join()

    #insert_part(output, dbuser, dbpass, dbhost, dbname)

    #p = Process(target=insert, args=(output,dbuser,dbpass,dbhost,dbname,),daemon=False)
    #p.start()
    #p.join()



    #logger.debug('End inserting')

    #[from_influx_line_proto_to_inserts_and_types('monitoring',line) for line in stor_var.splitlines()]


def insert(output_csv,dbuser,dbpass,dbhost,dbname,schema_inserts)->None:
    #logger.info(f"Start to insert to SQL. {current_thread().ident} ")
    from clickhouse_driver import client
    credentials = str()
    dbhost_dbname = str()
    if "".__ne__(dbpass):
        credentials = f"{dbuser}:{dbuser}@"
    else:
        credentials = f'{dbuser}@'
    if "".__ne__(dbname):
        dbhost_dbname = f'{dbhost}/{dbname}'
    else:
        dbhost_dbname = f'{dbhost}'
    client = client.Client.from_url(f"clickhouse://{credentials}{dbhost_dbname}")

    if len(schema_inserts)>0:
        logger.info("Creating tables")
        for insert in schema_inserts:
            try:
                client.execute(insert)
            except Exception as e:
                logger.info(str(e))
    try:
        client.execute(output_csv)
        #logger.info(f"Written: {client.rowcount}")
        #logger.info(f"Written: {client.receive_result()}")
    except Exception as e:
        logger.info(str(e))
        logger.info(output_csv[:1000])

    #logger.info(f"Enf of insert to SQL.  {current_thread().ident} ")
###########################################################
# get schema

def get_db_scheme(dbname,dbhost,dbuser,dbpassword) -> None:
    logger.info("Getting schema")
    global DATABASE_SCHEMA
    from clickhouse_driver import connect
    conn = str()
    if "".__eq__(dbpassword):
        conn = connect(f"clickhouse://{dbhost}/")
    else:
        conn = connect(user=dbuser, password=dbpassword,
                           host=dbhost, database=dbname)
    cursor = conn.cursor()
    query = f'SHOW TABLES FROM `{dbname}`'
    cursor.execute(query)
    tables = []
    for table in cursor.fetchall():
        tables.append(table[0])
    dict_tables = {}
    for table in tables:
        if table not in dict_tables.keys():
            dict_tables[table] = []
        query = f'describe table `{dbname}`.`{table}`'
        cursor.execute(query)
        dict_tables[table] = []
        for row in cursor.fetchall():
            dict_tables[table].append(row[0])
    conn.close()
    DATABASE_SCHEMA=dict_tables


###########################################################
# Start part

def cmd_arguments():
    parser = ArgumentParser()
    parser.add_argument("--server-ip", dest="serv_ip",
                        help="server ip address to bind, 127.0.0.1 if not defined", required=False, default='127.0.0.1')
    parser.add_argument("--server-port", dest="serv_port",
                        help="udp server port to bind, default 9150", required=False, type=int, default=9100)
    parser.add_argument("--server-buffer-size", dest="serv_buff",
                        help="Size of one read from buffer, default - sysctl net.core.rmem_default or sysctl net.core.rmem", required=False, type=int,
                        default=-1)
    parser.add_argument("--cmd-ip", dest="cmd_ip",
                        help="Command interface ip address to bind, 127.0.0.1 if not defined.", required=False,
                        default='127.0.0.1')
    parser.add_argument("--cmd-port", dest="cmd_port",
                        help="Command interface port to bind, 9101 if not defined", required=False,
                        default='9101')
    parser.add_argument("-l", "--log-level", dest="loglevel",
                        help="log level, DEBUG, INFO, WARNING, ERROR, CRITICAL, default NOTSET.", required=False,
                        default='NOTSET')
    parser.add_argument("--read-from-buff-interval", dest="readbufftime",
                        help="interval to flush BUFFER to parser, default 10 second", required=False, type=int,
                        default = 10)
    parser.add_argument("--dbname", dest="dbname",
                        help="Mariadb database name", required=True,
                        default='NOTSET')
    parser.add_argument("--dbhost", dest="dbhost",
                        help="mariadb host and port as 127.0.0.1:3306", required=True,
                        default='NOTSET')
    parser.add_argument("--dbuser", dest="dbuser",
                        help="login to mariadb", required=True,
                        default='NOTSET')
    parser.add_argument("--dbpass", dest="dbpass",
                        help="password", required=True,
                        default='NOTSET')
    args = parser.parse_args()
    SERV_IP = vars(args)['serv_ip']
    SERV_PORT = vars(args)['serv_port']
    SERV_BUFF = vars(args)['serv_buff']
    LOGLEVEL = vars(args)['loglevel']
    CMD_IP = vars(args)['cmd_ip']
    CMD_PORT = vars(args)['cmd_port']
    readbufftime = vars(args)['readbufftime']
    dbname = vars(args)['dbname']
    dbhost = vars(args)['dbhost']
    dbuser = vars(args)['dbuser']
    dbpass = vars(args)['dbpass']
    return SERV_IP,SERV_PORT,SERV_BUFF,LOGLEVEL,CMD_IP,CMD_PORT,readbufftime,dbname,dbhost,dbuser,dbpass

def main():
    SERV_IP,SERV_PORT,SERV_BUFF,LOGLEVEL,CMD_IP,CMD_PORT,readbufftime,dbname,dbhost,dbuser,dbpass = cmd_arguments()
    logger.setLevel(LOGLEVEL)

    t2 = Thread(target=parser, args=(dbname,dbhost,dbuser,dbpass,readbufftime,),daemon=True)
    t2.start()

    t3 = Thread(target=get_db_scheme,args=(dbname,dbhost,dbuser,dbpass,), daemon=True)
    t3.start()
    t3.join()

    t4 = Thread(target=statistics, daemon=True)
    t4.start()

    udp_server(SERV_IP,SERV_PORT,SERV_BUFF)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()

###########################################################
# DATA Generator
"""
import time
from line_protocol_parser import parse_line as parse_influx_line
import pandas as pd
import random
inf_list = []
timestamp = time.time_ns()
time.sleep(1)
time_append = time.time_ns() - timestamp
for i in range(10000):
    timestamp = timestamp + time_append
    for_bool = random.choice([True, False])
    for_int = int(random.random()*1000)
    double_quotes = '""'
    temp = parse_influx_line(f"cpu,host=qwe{double_quotes}rty,cpuid=#{int(random.random()*10)} count={for_int},flag={for_bool},cpu_us={random.random()*100},cpu_sy={random.random()*10} {timestamp}")
    inf_list.append({**temp['tags'],**temp['fields'],**{'temp_timestamp':temp['time']}})
df = pd.DataFrame(inf_list)
df['timestamp'] = df['temp_timestamp'].apply(lambda x: datetime.fromtimestamp(x / 1e9))
df.drop('temp_timestamp', axis=1, inplace=True)
types = {}
for col_name in df:
    if df.dtypes[col_name] == np.object:
        types[col_name] = 'String'
    elif df.dtypes[col_name] == np.float64:
        types[col_name] = 'Float64'
    elif df.dtypes[col_name] == np.bool:
        df[col_name] = df[col_name].replace({True: 1, False: 0})
        types[col_name] = 'UInt8'
    elif df.dtypes[col_name] == np.dtype('datetime64[ns]'):
        types[col_name] = 'DateTime64'
    else:
        types[col_name] = 'String'
#types['timestamp'] = 'DateTime64'
print(pd.io.sql.get_schema(df,'cpu',dtype=types))

def sql_generate_inserts(df, dbname, table):
    output = StringIO()
    df.iloc[:10].to_csv(output,index=False,header=False)
    return output.getvalue()
print(sql_generate_inserts(df,'monitoring','cpu'))
"""
###########################################################
# DISABLED FUNTIONS
#async def simple_add_message_to_list(reader,writer):
#    data = await reader.readline()
#    message = data.decode()
#    GLOBAL_LIST.append(message)
#
#async def from_buffer_to_memory():
#    global BUFFER_LIST
#    global GLOBAL_LIST
#    temp = BUFFER_LIST.copy()
#    while True:
#        await asyncio.sleep(30)
#        GLOBAL_LIST = GLOBAL_LIST + temp
#        for i in range(len(temp)):
#            BUFFER_LIST.pop(0)
###########################################################
# parser from influx line proto to arrays
# measurement - name or array
# tags, timestamp - fields - columns
#
#
#def to_sql_type(value) -> str:
#    if isinstance(value, float):
#        return 'FLOAT'
#    elif isinstance(value, int):
#        return 'INT'
#    elif isinstance(value, bool):
#        return 'BOOLEAN'
#    else:
#        return 'VARCHAR(255)'
#
#def from_influx_line_proto_to_inserts_and_types(dbname,dict) -> tuple:
#    """
#     :param dict: dict from line in influx proto
#     :return:
#     """
#    temp = dict
#    table = temp['measurement']
#    timestamp = temp['time']
#    # replace single quotes ' and backslashes \
#    tag_values = [str(x).replace("\\", "\\\\").replace("\'", "\\'") for x in temp['tags'].values()]
#    fields_values = [str(x).replace("\\", "\\\\").replace("\'", "\\'") for x in temp['fields'].values()]
#
#    tag_types = [to_sql_type(x) for x in temp['tags'].values()]
#    fields_types = [to_sql_type(x) for x in temp['fields'].values()]
#
#    tag_columns = list(temp['tags'].keys())
#    fields_columns = list(temp['fields'].keys())
#    # Search tag keys and fields with same name and rename fields
#    same_names = list(set(tag_columns) & set(fields_columns))
#    if len(same_names):
#        for same_name in same_names:
#            i = fields_columns.index(same_name)
#            fields_columns[i] = fields_columns[i] + "_field"
#
#    # prepare columns and values
#    columns_temp = tag_columns + fields_columns + ['timestamp']
#    columns = [f'`{x}`' for x in columns_temp]
#    values = [f"'{x}'" for x in tag_values + fields_values + [timestamp]]
#
#    # prepare types
#    types_temp = tag_types + fields_types + ['TIMESTAMP']
#    columns_and_types = [(columns_temp[i], types_temp[i]) for i in range(len(columns_temp))]
#
#    return (table, columns_and_types, "\n".join([f"INSERT INTO `{dbname}`.`{table}`",
#                                                f"({','.join(columns)})",
#                                                "VALUES",
#                                                f"({','.join(values)})"
#                                                ]))
#
#
#
###########################################################
# insert part
#
#
#def prepare_insert_tables_columns(table_to_insert, columns_and_types, current_scheme,dbname) -> tuple:
#    inserts_meta = []
#    insert_scheme = {}  # if creation of tale and column will be successfull, then add it to current scheme
#    #logger.debug(f"type of table_to_insert: { table_to_insert.__class__ } . {table_to_insert}")
#    #logger.debug(f"type of columns_and_types: {columns_and_types.__class__} . {columns_and_types}")
#    if table_to_insert in current_scheme.keys():
#        # Compare only column names< assume that same column name has same type
#        columns_name = [x[0] for x in columns_and_types]
#        #logger.debug(
#        #     f"Before adding new columns. Current_scheme: {current_scheme[table_to_insert]} , Scheme from insert {columns_and_types}.")
#        #logger.debug(f"columns_name: {columns_name.__class__} . current_scheme: {current_scheme.__class__} . table_to_insert : {table_to_insert}")
#        columns_to_add = list(set(columns_name) - set(current_scheme[table_to_insert]))
#        if len(columns_to_add) > 0:
#            logger.debug(f"columns_to_add {columns_to_add}")
#            insert_scheme[table_to_insert] = []
#            for colunm, type in columns_to_add:
#                inserts_meta.append(
#                    f"""ALTER TABLE `{dbname}`.`{table_to_insert}` ADD COLUMN `{colunm}` {type}""")
#                insert_scheme[table_to_insert].append((colunm, type))
#    else:
#        temp = ",".join(["`" + str(name) + "` " + str(type) for name, type in columns_and_types])
#        inserts_meta.append(
#            f"CREATE TABLE IF NOT EXISTS `{dbname}`.`{table_to_insert}` ({temp}) ENGINE=ColumnStore")
#        insert_scheme[table_to_insert] = columns_and_types
#
#    return (inserts_meta, insert_scheme)
#
#def get_db_scheme(dbname,dbhost,dbuser,dbpassword) -> dict:
#    conn = pymysql.connect(user=dbuser, password=dbpassword,
#                           host=dbhost, database=dbname)
#    cursor = conn.cursor()
#    query = f'SHOW TABLES FROM `{dbname}`'
#    cursor.execute(query)
#    tables = []
#    for table in cursor.fetchall():
#        # print(table[0])
#        tables.append(table[0])
#    dict_tables = {}
#    for table in tables:
#        if table not in dict_tables.keys():
#            dict_tables[table] = []
#        query = f'SHOW COLUMNS FROM `{dbname}`.`{table}`'
#        cursor.execute(query)
#        dict_tables[table] = []
#        for row in cursor.fetchall():
#            dict_tables[table].append(row[0])
#    conn.close()
#    return dict_tables
#
#def insert_meta(dbname,dbhost,dbuser,dbpassword,inserts,current_scheme,insert_scheme) -> dict:
#    conn = pymysql.connect(user=dbuser, password=dbpassword,
#                           host=dbhost, database=dbname)
#    cursor = conn.cursor()
#    for insert in inserts:
#        try:
#            cursor.execute(insert)
#            conn.commit()
#            table = list(insert_scheme.keys())[0]
#            if table not in current_scheme.keys():
#                current_scheme[table] = []
#            current_scheme[table].append( list(insert_scheme.values() ))
#        except Exception as e:
#            logger.error(f'Creating tables or adding columns with nsert "{insert}". {str(traceback.format_exc())}')
#    conn.close()
#    return current_scheme
#
#def insert(dbname,dbhost,dbuser,dbpass,parsed) -> None:
#    logger.info(f"Start insert, arguments: {dbname},{dbhost},{dbuser},{dbpass},{len(parsed)}")
#    start_time = time.time()
#    current_scheme = get_db_scheme(dbname, dbhost, dbuser, dbpass)
#
#    clean_data_inserts = []
#    for parsed_element in parsed:
#        table_to_insert, columns_and_types, insert_data = parsed_element
#        clean_data_inserts.append(insert_data)
#
#        inserts_meta, insert_scheme = prepare_insert_tables_columns(table_to_insert,
#                                                                    columns_and_types,
#                                                                    current_scheme,dbname)
#
#        if len(inserts_meta) > 0:
#            current_scheme = insert_meta(dbname, dbhost, dbuser, dbpass, inserts_meta, current_scheme, insert_scheme)
#
#    insert_thread = []
#    previous = 0
#    for i in range(500,len(clean_data_inserts),500):
#        previous = i
#        t = Thread(target=clean_inserts, args=(dbuser,dbpass,dbhost,dbname,clean_data_inserts[previous:i],), daemon=True)
#        t.start()
#        insert_thread.append(t)
#
#    t_last = Thread(target=clean_inserts, args=(dbuser, dbpass, dbhost, dbname, clean_data_inserts[previous:len(clean_data_inserts)],), daemon=True)
#    t_last.start()
#    insert_thread.append(t_last)
#
#    for index, thread in enumerate(insert_thread):
#        thread.join()
#
#    logger.debug(f"inserting time: {time.time() - start_time}")
#
#def clean_inserts(dbuser,dbpass,dbhost,dbname,inserts):
#    conn = pymysql.connect(user=dbuser, password=dbpass,
#                           host=dbhost, database=dbname)
#
#    cursor = conn.cursor()
#    for insert in inserts:
#        cursor.execute(insert)
#    conn.commit()
#    conn.close()
