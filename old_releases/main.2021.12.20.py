import sys
from threading import Thread,RLock
import logging
import signal
import time
from nats_simple import nats_simple
from arguments import Configuration
from influx_parser_writer import ParserLoader
from line_protocol_parser import parse_line as parse_influx_line
import queue
from pympler import asizeof

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

BUFFER = bytearray()
PARSER_QUEUES = {}
PARSER_THREADS = {}
WRITER_QUEUE = queue.Queue()
lock = RLock()

def sizeof_fmt(num, suffix='B'):
    ''' by Fred Cirera,  https://stackoverflow.com/a/1094933/1870254, modified'''
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%3.1f %s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f %s%s" % (num, 'Yi', suffix)

def writer(clickhouse,queue_to_write,writer_thread_id):
    logger.debug(f'writer thread started number {writer_thread_id}')
    while True:
        start_time = time.time()
        if queue_to_write.qsize() > 0:
            clickhouse.writer(read_all_from_queue(WRITER_QUEUE))

        #logger.debug(f"writer id {writer_thread_id} clickhouse obj for write: {asizeof.asizeof(clickhouse)}")
        running_time = time.time() - start_time
        if running_time < 1:
            time.sleep(1 - running_time)

def parser(clickhouse,queue_to_read,queue_to_write,table_name):
    logger.debug(f"{table_name} - parsing thread started")
    while True:
        start_time = time.time()
        #logger.debug(f"{table_name} queue size {asizeof.asizeof(queue_to_read)}, clickhouse obj for parse: {asizeof.asizeof(clickhouse)}")
        elements = read_all_from_queue(queue_to_read)
        if len(elements) > 0:
            queue_to_write.put(clickhouse.parser(elements, table_name))

        running_time = time.time() - start_time
        if running_time < 1:
            time.sleep(1 - running_time)

def read_all_from_queue(queue):
    output = list()
    while not queue.empty():
        output.append(queue.get())
        queue.task_done()
    return output

def callback_msg(msg):
    try:
        msg = msg.payload.decode()
    except Exception as e:
        pass
    parsed_json = {}
    try:
        parsed_json = parse_influx_line(msg)
    except Exception as e:
        logger.debug(f"Converting from influx line proto to dict: {str(e)} - {msg}")
        return None
    if (parsed_json['measurement'] not in PARSER_QUEUES.keys()):
        with lock:
            PARSER_QUEUES[parsed_json['measurement'] ] = queue.Queue()
    new_tags = {f"tag_{k}": v for k, v in parsed_json['tags'].items()}
    PARSER_QUEUES[parsed_json['measurement']].put({**new_tags, **parsed_json['fields'], **{'temp_timestamp': parsed_json['time']}})

def start_nats_thread(arg_tuple):
    (LOGLEVEL,
     readfromnats, dbname, dbsecure, dbhost, dbuser, dbpass, ttl, table_engine, chcluster,
     nats_servers, nats_scheme, path_to_nats_bin, client_name, server_stream,
     client_subject, writers_count, mem_stat, stdin_input) = arg_tuple

    stop_nats_thread = False
    nats_threads = []
    for nats_server in nats_servers:
        nats = nats_simple(callback_msg=callback_msg,
                              loglevel=LOGLEVEL,
                              nats_server=nats_server,
                              nats_scheme=nats_scheme,
                              client_name=client_name,
                              server_stream=server_stream,
                              client_subject=client_subject,
                              reconnect_timeout=10,
                              timeout_to_read_from_nats_stream=readfromnats)
        nats_t1 = Thread(target=nats.start, args=(lambda: stop_nats_thread,))
        nats_threads.append(nats_threads)
        nats_t1.start()

    return nats_threads

def start_stdin_read():
    for line in sys.stdin:
        if not line.startswith('[') and line.strip() != '':
            callback_msg(line)

def main():
    get_configuration = Configuration()
    """(LOGLEVEL,
     readbufftime,dbname,dbsecure,dbhost,dbuser,dbpass,ttl,table_engine,chcluster,
     nats_servers,nats_scheme,path_to_nats_bin,client_name,server_stream,
     client_subject,writers_count,mem_stat) = get_configuration.cmd_arguments()
     """
    arg_tuple = get_configuration.cmd_arguments()

    (LOGLEVEL,
     readfromnats, dbname, dbsecure, dbhost, dbuser, dbpass, ttl, table_engine, chcluster,
     nats_servers, nats_scheme, path_to_nats_bin, client_name, server_stream,
     client_subject, writers_count, mem_stat, stdin_input) = arg_tuple

    if LOGLEVEL == 'DEBUG':
        logger.setLevel(logging.DEBUG)
    elif LOGLEVEL == 'INFO':
        logger.setLevel(logging.INFO)
    elif LOGLEVEL == 'ERROR':
        logger.setLevel(logging.ERROR)
    elif LOGLEVEL == 'CRITICAL':
        logger.setLevel(logging.CRITICAL)
    elif LOGLEVEL == 'WARNING':
        logger.setLevel(logging.WARNING)
    else:
        logger.setLevel(logging.NOTSET)

    clickhouse_to_parse = ParserLoader(LOGLEVEL,dbname,dbsecure,dbhost,dbuser,dbpass,ttl,table_engine,chcluster)

    clickhouse_to_parse.connect_to_db()
    clickhouse_to_parse.get_db_scheme()

    writer_threads_list = []
    writer_clickhouse_connections = []

    # запускаем треды для записи
    for writer_thread_id in range(writers_count):
        clickhouse_to_write = ParserLoader(LOGLEVEL, dbname, dbsecure, dbhost, dbuser, dbpass, ttl, table_engine,
                                           chcluster)
        clickhouse_to_write.connect_to_db()
        writer_clickhouse_connections.append(clickhouse_to_write)
        writer_thread = Thread(target=writer,args=(clickhouse_to_write,WRITER_QUEUE,writer_thread_id,))
        writer_threads_list.append(writer_thread)
        writer_thread.start()

    if stdin_input:
        start_stdin_read_thread = Thread(target=start_stdin_read)
        start_stdin_read_thread.start()
    elif len(nats_servers) > 0:
        nats_threads = start_nats_thread(arg_tuple)
    else:
        sys.exit('Input type does not selected, need --nats-servers or --stdin')

    for_statistic_timestamp = time.time()
    while True:
        start_time = time.time()
        """
        if not nats_thread.is_alive():
            nats_thread = start_nats_thread(arg_tuple)
        start_time = time.time()
        """
        try:
            with lock:
                for table_name in PARSER_QUEUES.keys():
                    if table_name not in PARSER_THREADS.keys():
                        PARSER_THREADS[table_name] = Thread(target=parser, args=(clickhouse_to_parse,PARSER_QUEUES[table_name],WRITER_QUEUE,table_name,))
                        PARSER_THREADS[table_name].start()

        except Exception as e:
            logger.info(f'Error with something in starting parser threads {str(e)}')

        #logger.debug(f"WRITER queue size {asizeof.asizeof(WRITER_QUEUE)}, "
        #             f"writer_threads_list: {asizeof.asizeof(writer_threads_list)}, "
        #             f"writer_clickhouse_connections {asizeof.asizeof(writer_clickhouse_connections)}")

        if mem_stat and (time.time() - for_statistic_timestamp > 60):
            for name, size in sorted(((name, asizeof.asizeof(value)) for name, value in locals().items()),
                                     key=lambda x: -x[1])[:10]:
                logger.info("{:>30}: {:>8}".format(name, sizeof_fmt(size)))
            for i in range(writers_count):
                logger.info(f"writer_threads_list id {i}: {sizeof_fmt(asizeof.asizeof(writer_threads_list[i]))}")
                logger.info(f"writer_clickhouse_connections id {i}: {sizeof_fmt(asizeof.asizeof(writer_clickhouse_connections[i]))}")

            logger.info(f"WRITER_QUEUE: {sizeof_fmt(asizeof.asizeof(WRITER_QUEUE))}")
            with lock:
                for table_name in PARSER_QUEUES.keys():
                    logger.info(f"PARSER_QUEUES[{table_name}]: {sizeof_fmt(asizeof.asizeof(PARSER_QUEUES[table_name]))}")
                    if table_name in PARSER_THREADS.keys():
                        logger.info(
                            f"PARSER_THREADS[{table_name}]: {sizeof_fmt(asizeof.asizeof(PARSER_THREADS[table_name]))}")

            for_statistic_timestamp = time.time()

        running_time = time.time() - start_time
        if running_time < 1:
            time.sleep(1 - running_time)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()
