# дальше вот так (созадаим loop в треде(нити))
# https://gist.github.com/dmfigol/3e7d5b84a16d076df02baa9f53271058
#
# udo server from
# https://pythontic.com/modules/socket/udp-client-server-example
#
##############################################################
#
# Запукает два треда, синхронный UDP сервер в основном треде, в другом запускат asyncio. Там где asyncio идёт сохранение данных
# из UDP сервера. В качестве переменных для буфера между UDP сервером и сохранением данных используется bytearray,
# потому что потребляет мало памяти, но при этом почему-то больше чем 1711271416 байт (но я думаю 2ГБ по факту), поместить в
# переменную не получается.
# Есть 2 нюанса:
# 1. Буфер отчищается командой del[:индекс], очень круто и быстро
# 2. Из-за ограничений на размер переменной bytearray, пришлось создавать глобальные переменные с числовым суффиком,
#    например STORAGE_0, STORAGE_1 и так далее. Команда вида globals()['имя переменной'] отлично для этого подходит.
# 3. Новые переменные STORAGE создаются, когда старая + новые данные из буфера, становятся больше 1 ГБ -
#    задётся через SIZE_OF_ONE_STORAGE_BYTES
# 4. переменные сжимаются через zlib, гигабайт за 20 секунд сжимался
#
# Результат:
# 1. читайет по 500к-600к строк за 10 секунд без проблем
# 2. Cжимает хорошо, 30-50 МБ из 1 ГБ, час сжатых метрик ~ 2ГБ в ОЗУ, это примерно 72 814 960 строк
#
# что с этим делать дальше пока не ясно

from threading import Thread
import asyncio
import logging
import sys
from argparse import ArgumentParser
from datetime import datetime
import asyncio_dgram
import traceback
import io
from pympler import asizeof
from array import array
import signal
import socket
import time
import zlib
from line_protocol_parser import parse_line as parse_influx_line
import pymysql

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

BUFFER = bytearray()
DICT_STORAGE = dict()
VARIABLE_PREFIX_FOR_STORAGE = "STORAGE_"
globals()[VARIABLE_PREFIX_FOR_STORAGE + "0"] = bytearray()
LINES_COUNT = 0
SIZE_OF_ONE_STORAGE_BYTES = 1073741824 # 1 GB
#SIZE_OF_ONE_STORAGE_BYTES = 10*1024 # 10 kB

###############################################################
# ACTIVE FUNCTIONS

class mariadb_part:
    def __init__(self,DATABASE, MARIADB_SERVER, MARIADB_USER, MARIADB_PASSWD):
        self.DATABASE = DATABASE
        self.MARIADB_SERVER = MARIADB_SERVER
        self.MARIADB_USER = MARIADB_USER
        self.MARIADB_PASSWD = MARIADB_PASSWD

    def connect(self):
        self.conn = pymysql.connect(user=self.MARIADB_USER, password=self.MARIADB_PASSWD,
                                             host=self.MARIADB_SERVER, database=self.DATABASE)
        self.cursor = self.conn.cursor()
        self.current_scheme = self.get_tables_columns()
        return self.conn,self.cursor

    def get_tables_columns(self):
        query = f'SHOW TABLES FROM `{self.DATABASE}`'
        self.cursor.execute(query)
        tables = []
        for table in self.cursor.fetchall():
            # print(table[0])
            tables.append(table[0])
        dict_tables = {}
        for table in tables:
            if table not in dict_tables.keys():
                dict_tables[table] = []
            query = f'SHOW COLUMNS FROM `{self.DATABASE}`.`{table}`'
            self.cursor.execute(query)
            dict_tables[table] = []
            for row in self.cursor.fetchall():
                dict_tables[table].append((row[0], str(row[1]).upper()))
        return dict_tables

    def to_sql_type(self,value):
        if isinstance(value, float):
            return 'FLOAT'
        elif isinstance(value, int):
            return 'INT'
        elif isinstance(value, bool):
            return 'BOOLEAN'
        else:
            return 'VARCHAR(255)'

    def from_influx_line_proto_to_inserts_and_types(self,json_from_line_proto):
        """
        :param json: something like this -
        {'measurement': 'myMeas',
        'fields': {'field1': 3.14, 'field2': 'Hello, World!'},
        'tags': {'someTag': 'ABC'},
        'time': 123}
        :return:
        """
        temp = json_from_line_proto
        table = temp['measurement']
        timestamp = temp['time']
        # replace single quotes ' and backslashes \
        tag_values = [str(x).replace("\\", "\\\\").replace("\'", "\\'") for x in temp['tags'].values()]
        fields_values = [str(x).replace("\\", "\\\\").replace("\'", "\\'") for x in temp['fields'].values()]

        tag_types = [ self.to_sql_type(x)  for x in temp['tags'].values()]
        fields_types = [self.to_sql_type(x) for x in temp['fields'].values()]

        tag_columns = list(temp['tags'].keys())
        fields_columns = list(temp['fields'].keys())
        # Search tag keys and fields with same name and rename fields
        same_names = list(set(tag_columns) & set(fields_columns))
        if len(same_names):
            for same_name in same_names:
                i = fields_columns.index(same_name)
                fields_columns[i] = fields_columns[i] + "_field"

        # prepare columns and values
        columns_temp = tag_columns + fields_columns + ['timestamp']
        columns = [f'`{x}`' for x in columns_temp]
        values = [f"'{x}'" for x in tag_values + fields_values + [timestamp]]

        # prepare types
        types_temp = tag_types+fields_types+['TIMESTAMP']
        columns_and_types = [ (columns_temp[i],types_temp[i]) for i in range(len(columns_temp))]

        return table,columns_and_types, "\n".join([f"INSERT INTO `{self.DATABASE}`.`{table}`",
                       f"({','.join(columns)})",
                       "VALUES",
                       f"({','.join(values)})"
                       ])

    def create_tables(self,table_name,columns_types):
        temp = ",".join([ "`" + str(name) + "` " + str(type) for name, type in columns_types])
        try:
            self.cursor.execute(f"CREATE TABLE IF NOT EXISTS `{self.DATABASE}`.`{table_name}` ({temp}) ENGINE=ColumnStore")
            self.conn.commit()
            return True
        except Exception as e:
            logger.error(f"Can't create table `{self.DATABASE}`.`{table_name}`. Error {str(traceback.format_exc())}")
            return False

    def add_columns(self,table,columns_to_add):
        check = True
        for colunm,type in columns_to_add:
            temp = f"""ALTER TABLE `{self.DATABASE}`.`{table}` ADD COLUMN `{colunm}` {type}"""
            try:
                self.cursor.execute(temp)
                self.conn.commit()
            except Exception as e:
                logger.error(f"adding column {temp}. {str(traceback.format_exc())}")
                check = False
        return check

    def insert_data_without_commit(self,json_from_line_proto):
        table_to_insert,columns_types_to_insert, insert = self.from_influx_line_proto_to_inserts_and_types(json_from_line_proto)
        current_scheme = self.get_tables_columns()
        check = True
        if not table_to_insert in current_scheme.keys():
            check = self.create_tables(table_to_insert,columns_types_to_insert)
        else:
            logger.debug(f"Before adding new columns. Current_scheme: {current_scheme[table_to_insert]} , Scheme from insert {columns_types_to_insert}.")
            columns_to_add = list(set(columns_types_to_insert) - set(current_scheme[table_to_insert]))
            if len(columns_to_add) > 0:
                check = self.add_columns(table_to_insert,columns_to_add)
        if not check:
            logger.error(f"ERROR. Check previous errors")
            return
        try:
            self.cursor.execute(insert)
        except Exception as e:
            logger.error(f"ERROR inserting: {str(traceback.format_exc())}. Insert {insert} ")

    def commit(self):
        self.conn.commit()

    def close_connection(self):
        self.conn.commit()
        self.conn.close()

async def read_from_buffer_to_db(dbname,dbhost,dbuser,dbpassword):
    global BUFFER
    global DICT_STORAGE
    global LINES_COUNT
    mariadb = mariadb_part(dbname,dbhost,dbuser,dbpassword)
    mariadb.connect()
    while True:
        # for BUFFER is LIST type
        start_time = time.time()
        #print(f"len BUFFER: {len(BUFFER)}")
        new_line_last_index = BUFFER.rfind(b"\n")
        if new_line_last_index > 0:
            element = BUFFER[:new_line_last_index]
            del BUFFER[:new_line_last_index]
            inserts = []
            for line in element.splitlines():
                line = line.decode()
                # from influx line protocol to dictionary
                temp = {}
                try:
                    temp = parse_influx_line(line)
                except Exception as e:
                    logger.error(f"ERROR: {str(traceback.format_exc())} - {line}")
                    #continue
                # If there is no timestamp at all, but we received message
                if temp['time'] == 0:
                    temp['time'] = time.time_ns()
                temp['time'] = (datetime.fromtimestamp(temp['time'] / 1e9)).strftime('%Y-%m-%d %H:%M:%S.%f')[:-4]

                mariadb.insert_data_without_commit(temp)

            mariadb.commit()
            LINES_COUNT = LINES_COUNT + len(element.splitlines())
        running_time = time.time() - start_time
        #print(f"running_time: {running_time}")
        # always wait 10 seconds
        if running_time < 1.0 :
            await asyncio.sleep(1.0 - running_time)
    mariadb.close_connection()

async def print_list_count_and_size():
    global BUFFER
    global LINES_COUNT

    #header csv
    header = ",".join(["time",
                       "elements",
                       "buffer pympler size"
                       ])
    logger.info(header)
    while True:
        await asyncio.sleep(10)
        # Get last storage variable number
        storage_variables_nums = [int(x[len(VARIABLE_PREFIX_FOR_STORAGE):]) for x in globals().keys() if
                                  x.startswith(VARIABLE_PREFIX_FOR_STORAGE)]
        storage_variables_nums.sort()
        storage_variable_last_num = storage_variables_nums[-1]

        compressed_storages_size = int()
        for num in storage_variables_nums[:-1]:
            compressed_storages_size = compressed_storages_size + \
                                       asizeof.asizeof(globals()[VARIABLE_PREFIX_FOR_STORAGE + str(num)])

        line = ",".join([str(datetime.now().strftime("%Y.%m.%d %H:%M:%S")),
                         str(LINES_COUNT),
                         str(asizeof.asizeof(BUFFER))
                         ])
        logger.info(line)

async def command_listener(reader, writer):
    data = await reader.read(1024)
    command = data.decode()
    addr = writer.get_extra_info('peername')

    logger.info(f"Received {command!r} from {addr!r}")

    result = str()
    ######################################
    # REDIRECT OUTPUT
    # keep a named handle on the prior stdout
    old_stdout = sys.stdout
    # keep a named handle on io.StringIO() buffer
    new_stdout = io.StringIO()
    # Redirect python stdout into the builtin io.StringIO() buffer
    sys.stdout = new_stdout

    try:
        exec(str(command).strip())
        result = sys.stdout.getvalue().strip()
    except Exception as e:
        result = str(traceback.format_exc())

    # put stdout back to normal
    sys.stdout = old_stdout
    # END OF REDIRECT
    ######################################
    logger.info(f"Send: {result}")
    writer.write(str.encode(f"Output: {result}\n"))
    await writer.drain()

    logger.info("Close the connection")
    writer.close()

def udp_server(IP,PORT,buffer_size):
    # Create a datagram socket
    UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    #get os buffer size
    if buffer_size <= 0:
        bufferSize = int(UDPServerSocket.getsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF))
    else:
        bufferSize = buffer_size
    # Bind to address and ip
    UDPServerSocket.bind((IP,PORT))
    logger.info(f"UDP server up and listening. {IP}:{PORT} ")
    # Listen for incoming datagrams
    while (True):
        bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
        BUFFER.extend(bytesAddressPair[0])

async def main_thread(cmd_ip, cmd_port,dbname,dbhost,dbuser,dbpass):
    cmd_server = await asyncio.start_server(command_listener, cmd_ip, cmd_port)
    await asyncio.gather(
        read_from_buffer_to_db(dbname,dbhost,dbuser,dbpass),
        print_list_count_and_size(),
        cmd_server.serve_forever(),
    )

def start_background_loop(loop: asyncio.AbstractEventLoop) -> None:
    asyncio.set_event_loop(loop)
    loop.run_forever()

def main():
    SERV_IP,SERV_PORT,SERV_BUFF,LOGLEVEL,CMD_IP,CMD_PORT,dbname,dbhost,dbuser,dbpass = cmd_arguments()
    logger.setLevel(LOGLEVEL)

    #loop = asyncio.new_event_loop()
    #t = Thread(target=start_background_loop, args=(loop,), daemon=True)
    udp_server_thread = Thread(target=udp_server, args=(SERV_IP,SERV_PORT,SERV_BUFF,), daemon=True)
    udp_server_thread.start()

    asyncio.run(main_thread(CMD_IP,CMD_PORT,dbname,dbhost,dbuser,dbpass))
    #asyncio.run_coroutine_threadsafe(run_in_thread(CMD_IP,CMD_PORT,dbname,dbhost,dbuser,dbpass), loop)


def cmd_arguments():
    parser = ArgumentParser()
    parser.add_argument("--server-ip", dest="serv_ip",
                        help="server ip address to bind, 127.0.0.1 if not defined", required=False, default='127.0.0.1')
    parser.add_argument("--server-port", dest="serv_port",
                        help="udp server port to bind, default 9150", required=False, type=int, default=9100)
    parser.add_argument("--server-buffer-size", dest="serv_buff",
                        help="Size of one read from buffer, default - sysctl net.core.rmem_default or sysctl net.core.rmem", required=False, type=int,
                        default=-1)
    parser.add_argument("--cmd-ip", dest="cmd_ip",
                        help="Command interface ip address to bind, 127.0.0.1 if not defined.", required=False,
                        default='127.0.0.1')
    parser.add_argument("--cmd-port", dest="cmd_port",
                        help="Command interface port to bind, 9101 if not defined", required=False,
                        default='9101')
    parser.add_argument("-l", "--log-level", dest="loglevel",
                        help="log level, DEBUG, INFO, WARNING, ERROR, CRITICAL, default NOTSET.", required=False,
                        default='NOTSET')
    parser.add_argument("--dbname", dest="dbname",
                        help="Mariadb database name", required=True,
                        default='NOTSET')
    parser.add_argument("--dbhost", dest="dbhost",
                        help="mariadb host and port as 127.0.0.1:3306", required=True,
                        default='NOTSET')
    parser.add_argument("--dbuser", dest="dbuser",
                        help="login to mariadb", required=True,
                        default='NOTSET')
    parser.add_argument("--dbpass", dest="dbpass",
                        help="password", required=True,
                        default='NOTSET')
    args = parser.parse_args()
    SERV_IP = vars(args)['serv_ip']
    SERV_PORT = vars(args)['serv_port']
    SERV_BUFF = vars(args)['serv_buff']
    LOGLEVEL = vars(args)['loglevel']
    CMD_IP = vars(args)['cmd_ip']
    CMD_PORT = vars(args)['cmd_port']
    DBNAME = vars(args)['dbname']
    DBHOST = vars(args)['dbhost']
    DBUSER = vars(args)['dbuser']
    DBPASSWORD = vars(args)['dbpass']
    return SERV_IP,SERV_PORT,SERV_BUFF,LOGLEVEL,CMD_IP,CMD_PORT,DBNAME,DBHOST,DBUSER,DBPASSWORD

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    asyncio.run(main())


###########################################################
# DISABLED FUNTIONS
#async def simple_add_message_to_list(reader,writer):
#    data = await reader.readline()
#    message = data.decode()
#    GLOBAL_LIST.append(message)
#
#async def from_buffer_to_memory():
#    global BUFFER_LIST
#    global GLOBAL_LIST
#    temp = BUFFER_LIST.copy()
#    while True:
#        await asyncio.sleep(30)
#        GLOBAL_LIST = GLOBAL_LIST + temp
#        for i in range(len(temp)):
#            BUFFER_LIST.pop(0)
