# дальше вот так (созадаим loop в треде(нити))
# https://gist.github.com/dmfigol/3e7d5b84a16d076df02baa9f53271058
#
# udo server from
# https://pythontic.com/modules/socket/udp-client-server-example
#
##############################################################
#
# Запукает два треда, синхронный UDP сервер в основном треде, в другом запускат asyncio. Там где asyncio идёт сохранение данных
# из UDP сервера. В качестве переменных для буфера между UDP сервером и сохранением данных используется bytearray,
# потому что потребляет мало памяти, но при этом почему-то больше чем 1711271416 байт (но я думаю 2ГБ по факту), поместить в
# переменную не получается.
# Есть 2 нюанса:
# 1. Буфер отчищается командой del[:индекс], очень круто и быстро
# 2. Из-за ограничений на размер переменной bytearray, пришлось создавать глобальные переменные с числовым суффиком,
#    например STORAGE_0, STORAGE_1 и так далее. Команда вида globals()['имя переменной'] отлично для этого подходит.
# 3. Новые переменные STORAGE создаются, когда старая + новые данные из буфера, становятся больше 1 ГБ -
#    задётся через SIZE_OF_ONE_STORAGE_BYTES
# 4. переменные сжимаются через zlib, гигабайт за 20 секунд сжимался
#
# Результат:
# 1. читайет по 500к-600к строк за 10 секунд без проблем
# 2. Cжимает хорошо, 30-50 МБ из 1 ГБ, час сжатых метрик ~ 2ГБ в ОЗУ, это примерно 72 814 960 строк
#
# что с этим делать дальше пока не ясно

from threading import Thread
import asyncio
import logging
import sys
from argparse import ArgumentParser
from datetime import datetime
import asyncio_dgram
import traceback
import io
from pympler import asizeof
from array import array
import signal
import socket
import time
import zlib

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

BUFFER = bytearray()
VARIABLE_PREFIX_FOR_STORAGE = "STORAGE_"
globals()[VARIABLE_PREFIX_FOR_STORAGE + "0"] = bytearray()
LINES_COUNT = 0
SIZE_OF_ONE_STORAGE_BYTES = 1073741824 # 1 GB
TIME_LAST_COMPRESS = 0
#SIZE_OF_ONE_STORAGE_BYTES = 10*1024 # 10 kB

###############################################################
# ACTIVE FUNCTIONS

def cmd_arguments():
    parser = ArgumentParser()
    parser.add_argument("--server-ip", dest="serv_ip",
                        help="server ip address to bind, 127.0.0.1 if not defined", required=False, default='127.0.0.1')
    parser.add_argument("--server-port", dest="serv_port",
                        help="udp server port to bind, default 9150", required=False, type=int, default=9100)
    parser.add_argument("--server-buffer-size", dest="serv_buff",
                        help="Size of one read from buffer, default - sysctl net.core.rmem_default or sysctl net.core.rmem", required=False, type=int,
                        default=-1)
    parser.add_argument("--cmd-ip", dest="cmd_ip",
                        help="Command interface ip address to bind, 127.0.0.1 if not defined.", required=False,
                        default='127.0.0.1')
    parser.add_argument("--cmd-port", dest="cmd_port",
                        help="Command interface port to bind, 9101 if not defined", required=False,
                        default='9101')
    parser.add_argument("-l", "--log-level", dest="loglevel",
                        help="log level, DEBUG, INFO, WARNING, ERROR, CRITICAL, default NOTSET.", required=False,
                        default='NOTSET')
    args = parser.parse_args()
    SERV_IP = vars(args)['serv_ip']
    SERV_PORT = vars(args)['serv_port']
    SERV_BUFF = vars(args)['serv_buff']
    LOGLEVEL = vars(args)['loglevel']
    CMD_IP = vars(args)['cmd_ip']
    CMD_PORT = vars(args)['cmd_port']
    return SERV_IP,SERV_PORT,SERV_BUFF,LOGLEVEL,CMD_IP,CMD_PORT

async def print_list_count_and_size():
    global BUFFER
    global LINES_COUNT
    global TIME_LAST_COMPRESS
    #header csv
    header = ",".join(["time",
                       "elements",
                       "buffer pympler size",
                       "total storage variables",
                       "actual storage size",
                       "compressed storages size",
                       "last compressed sec"
                       ])
    print(header)
    while True:
        await asyncio.sleep(10)
        # Get last storage variable number
        storage_variables_nums = [int(x[len(VARIABLE_PREFIX_FOR_STORAGE):]) for x in globals().keys() if
                                  x.startswith(VARIABLE_PREFIX_FOR_STORAGE)]
        storage_variables_nums.sort()
        storage_variable_last_num = storage_variables_nums[-1]

        compressed_storages_size = int()
        for num in storage_variables_nums[:-1]:
            compressed_storages_size = compressed_storages_size + \
                                       asizeof.asizeof(globals()[VARIABLE_PREFIX_FOR_STORAGE + str(num)])

        line = ",".join([str(datetime.now().strftime("%Y.%m.%d %H:%M:%S")),
                         str(LINES_COUNT),
                         str(asizeof.asizeof(BUFFER)),
                         str(storage_variable_last_num+1),
                         str(asizeof.asizeof(globals()[VARIABLE_PREFIX_FOR_STORAGE + str(storage_variable_last_num)])),
                         str(compressed_storages_size),
                         str(TIME_LAST_COMPRESS)
                         ])
        print(line)

async def command_listener(reader, writer):
    data = await reader.read(1024)
    command = data.decode()
    addr = writer.get_extra_info('peername')

    print(f"Received {command!r} from {addr!r}")

    result = str()
    ######################################
    # REDIRECT OUTPUT
    # keep a named handle on the prior stdout
    old_stdout = sys.stdout
    # keep a named handle on io.StringIO() buffer
    new_stdout = io.StringIO()
    # Redirect python stdout into the builtin io.StringIO() buffer
    sys.stdout = new_stdout

    try:
        exec(str(command).strip())
        result = sys.stdout.getvalue().strip()
    except Exception as e:
        result = str(traceback.format_exc())

    # put stdout back to normal
    sys.stdout = old_stdout
    # END OF REDIRECT
    ######################################
    print(f"Send: {result}")
    writer.write(str.encode(f"Output: {result}\n"))
    await writer.drain()

    print("Close the connection")
    writer.close()

async def read_from_buffer():
    global BUFFER
    global LINES_COUNT
    global TIME_LAST_COMPRESS
    while True:
        # for BUFFER is LIST type
        start_time = time.time()
        #print(f"len BUFFER: {len(BUFFER)}")
        new_line_last_index = BUFFER.rfind(b"\n")
        if new_line_last_index > 0:
            element = BUFFER[:new_line_last_index]
            del BUFFER[:new_line_last_index]

            #print(f"element: {element}")
            # Get last storage variable number
            storage_variables_nums = [int(x[len(VARIABLE_PREFIX_FOR_STORAGE):]) for x in globals().keys() if x.startswith(VARIABLE_PREFIX_FOR_STORAGE)]
            storage_variables_nums.sort()
            storage_variable_last_num = storage_variables_nums[-1]

            # Check if last storage variable would be greater 1GB if we add data from buffer
            # if so, then create new storage variable and compress previous
            #print(f"storage variable name: {VARIABLE_PREFIX_FOR_STORAGE+str(storage_variable_last_num)}")
            #print(f"storage variable value: {globals()[VARIABLE_PREFIX_FOR_STORAGE+str(storage_variable_last_num)]}")
            if (asizeof.asizeof(element) + asizeof.asizeof(globals()[VARIABLE_PREFIX_FOR_STORAGE+str(storage_variable_last_num)])) > SIZE_OF_ONE_STORAGE_BYTES:
                # compression
                #print(f"Compress {VARIABLE_PREFIX_FOR_STORAGE + str(storage_variable_last_num)}")
                start_time_compress = time.time()
                globals()[VARIABLE_PREFIX_FOR_STORAGE + str(storage_variable_last_num)] = zlib.compress(
                    globals()[VARIABLE_PREFIX_FOR_STORAGE + str(storage_variable_last_num)],level=1)
                TIME_LAST_COMPRESS = time.time() - start_time_compress
                # Create new variable
                #print(f"Create new: {VARIABLE_PREFIX_FOR_STORAGE + str(storage_variable_last_num)}")
                storage_variable_last_num = storage_variable_last_num + int(1)
                globals()[VARIABLE_PREFIX_FOR_STORAGE + str(storage_variable_last_num)] = bytearray()

            #extend currrent storage variable
            #print(f"Extend {VARIABLE_PREFIX_FOR_STORAGE + str(storage_variable_last_num)}")
            globals()[VARIABLE_PREFIX_FOR_STORAGE + str(storage_variable_last_num)].extend(element)
            #print(f"Save LINES_COUNT ")
            LINES_COUNT = LINES_COUNT + len(element.splitlines())
        running_time = time.time() - start_time
        #print(f"running_time: {running_time}")
        # always wait 10 seconds
        if running_time < 10.0 :
            await asyncio.sleep(10.0 - running_time)

def udp_server(IP,PORT,buffer_size):
    # Create a datagram socket
    UDPServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
    #get os buffer size
    if buffer_size <= 0:
        bufferSize = int(UDPServerSocket.getsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF))
    else:
        bufferSize = buffer_size
    # Bind to address and ip
    UDPServerSocket.bind((IP,PORT))
    print(f"UDP server up and listening. {IP}:{PORT} ")
    # Listen for incoming datagrams
    while (True):
        bytesAddressPair = UDPServerSocket.recvfrom(bufferSize)
        BUFFER.extend(bytesAddressPair[0])

async def run_in_thread(cmd_ip, cmd_port):
    cmd_server = await asyncio.start_server(command_listener, cmd_ip, cmd_port)
    await asyncio.gather(
        read_from_buffer(),
        print_list_count_and_size(),
        cmd_server.serve_forever(),
    )

def start_background_loop(loop: asyncio.AbstractEventLoop) -> None:
    asyncio.set_event_loop(loop)
    loop.run_forever()

def main():
    SERV_IP,SERV_PORT,SERV_BUFF,LOGLEVEL,CMD_IP,CMD_PORT = cmd_arguments()
    logger.setLevel(LOGLEVEL)

    loop = asyncio.new_event_loop()
    t = Thread(target=start_background_loop, args=(loop,), daemon=True)
    t.start()

    asyncio.run_coroutine_threadsafe(run_in_thread(CMD_IP,CMD_PORT), loop)

    udp_server(SERV_IP,SERV_PORT,SERV_BUFF)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    asyncio.run(main())


###########################################################
# DISABLED FUNTIONS
#async def simple_add_message_to_list(reader,writer):
#    data = await reader.readline()
#    message = data.decode()
#    GLOBAL_LIST.append(message)
#
#async def from_buffer_to_memory():
#    global BUFFER_LIST
#    global GLOBAL_LIST
#    temp = BUFFER_LIST.copy()
#    while True:
#        await asyncio.sleep(30)
#        GLOBAL_LIST = GLOBAL_LIST + temp
#        for i in range(len(temp)):
#            BUFFER_LIST.pop(0)
