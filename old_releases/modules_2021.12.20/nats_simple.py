from pynats import NATSClient
import time
import random
from subprocess import Popen, PIPE
import json
import socket
import re
import logging
from typing import Callable
from pathlib import Path

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

class nats_simple():
    def __init__(
                self,
                callback_msg: Callable,
                loglevel: str = "INFO",
                client_name: str = "nats-python",
                server_stream: str = "TELEGRAF",
                client_subject: str = "telegraf_client",
                nats_server: str = "127.0.0.1:4221",
                nats_scheme: str = "nats",
                reconnect_timeout: int = 2,
                timeout_to_read_from_nats_stream: float = 0.1
                ) -> None:
        logger.setLevel(loglevel)
        self.callback_msg = callback_msg
        self.client_subject = client_subject
        self.client_name = client_name
        self.server_stream = server_stream
        self.nats_server = nats_server
        self.nats_scheme = nats_scheme
        self.reconnect_timeout = reconnect_timeout
        self.timeout_to_read_from_nats_stream = timeout_to_read_from_nats_stream

    def start(self,stop):
        client = self.connect(self.nats_server)
        client.subscribe(subject=self.client_subject, callback=self.callback_msg)
        while True:
            if stop():
                break
            start_time = time.time()
            try:
                client.wait()
            except Exception as e:
                logger.info(f"Error {str(e)}.Reconnect to {self.nats_server}")
                time.sleep(self.reconnect_timeout)
                client = self.connect(self.nats_server )
                client.subscribe(subject=self.client_subject, callback=self.callback_msg)

            running_time = time.time() - start_time
            # default timeout_to_read_from_nats_stream = 1 second
            if running_time < self.timeout_to_read_from_nats_stream:
                time.sleep(self.timeout_to_read_from_nats_stream - running_time)

    def connect(self,server_to_connect) -> NATSClient:
        connected = False
        while not connected:
            start_time = time.time()
            client = NATSClient(url="".join([self.nats_scheme, '://', server_to_connect]))
            try:
                client.connect()
                connected = True
                logger.info(f"Connected {server_to_connect}")
                return client
            except Exception as e:
                logger.info(f"Can't connect to {server_to_connect}")
                running_time = time.time() - start_time
            if running_time < self.reconnect_timeout :
                time.sleep(self.reconnect_timeout - running_time)

    def callback_debug(self, msg):
        logger.debug(f"{msg}")
