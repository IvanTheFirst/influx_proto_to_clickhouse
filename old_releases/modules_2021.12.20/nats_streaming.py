from pynats import NATSClient
import time
import random
from subprocess import Popen, PIPE
import json
import socket
import re
import logging
from typing import Callable
from pathlib import Path

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

class nats_streaming():
    def __init__(
                self,
                callback_msg: Callable,
                loglevel: str = "INFO",
                client_name: str = "nats-python",
                server_stream: str = "TELEGRAF",
                client_subject: str = "telegraf_client",
                nats_servers: list = ["127.0.0.1:4221","127.0.0.1:4222","127.0.0.1:4223"],
                nats_scheme: str = "nats",
                path_to_nats_bin: str = "nats",
                timeout_for_failed_server: int = 60,
                timeout_for_one_server_online: int = 60,
                timeout_to_read_from_nats_stream: float = 0.1
                ) -> None:
        logger.setLevel(loglevel)
        self.callback_msg = callback_msg
        self.client_subject = client_subject
        self.client_name = client_name
        self.server_stream = server_stream
        self.nats_servers = nats_servers
        self.nats_scheme = nats_scheme
        self.path_to_nats_bin = path_to_nats_bin
        self.timeout_for_failed_server = timeout_for_failed_server
        self.timeout_for_one_server_online = timeout_for_one_server_online
        self.timeout_to_read_from_nats_stream = timeout_to_read_from_nats_stream

    def start(self,stop):
        servers_with_stream = self.check_stream_servers()
        client = self.connect(servers_with_stream)
        client.subscribe(subject=self.client_subject, callback=self.callback_msg)
        failed_servers = {}

        start_total_time = time.time()
        while True:
            if stop():
                break
            start_time = time.time()
            try:
                client.wait()
            except Exception as e:
                logger.info(e)
                logger.info("Reconnect")
                # adding server to failed with timeout_for_one_server_online timeout (default 60 seconds)
                failed_servers[f"{client._conn_options.hostname}:{client._conn_options.port}"] = time.time() + self.timeout_for_failed_server
                client = self.connect(servers_with_stream, servers_to_exclude=failed_servers)
                client.subscribe(subject=self.client_subject, callback=self.callback_msg)

            running_time = time.time() - start_time
            # default timeout_to_read_from_nats_stream = 1 second
            if running_time < self.timeout_to_read_from_nats_stream:
                time.sleep(self.timeout_to_read_from_nats_stream - running_time)

    def cli(self,server):
        cmd = [str(Path(self.path_to_nats_bin))]
        cmd.extend(['-s',server,'con','info',self.server_stream,self.client_name,'--json'])
        logger.debug(f"nats cli command:{cmd}")
        p = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE,encoding="utf8")
        output, err = p.communicate()
        logger.debug(f"output: {output}")
        if str(err) != '': logger.debug(f"error: {err}")
        return output, err

    def check_stream_servers(self):
        servers_with_stream = []
        servers_internal_names_ip = {}
        info_re = re.compile(rb"^INFO\s+([^\r\n]+)\r\n")
        while len(servers_with_stream) == 0:
            start_time = time.time()
            logger.debug("get internal server names")
            for server in self.nats_servers:
                ip, port = server.split(':')
                sock = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
                sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
                try:
                    sock.connect((ip, int(port)))
                except Exception as e:
                    continue
                response = sock.recv(128000)
                sock.close()
                temp_json_response = json.loads(info_re.findall(response)[0].decode())
                servers_internal_names_ip[temp_json_response['server_name']] = server

            logger.debug(f"get servers with replica with {self.server_stream}")
            for server in servers_internal_names_ip.values():
                server_to_connect = "".join([self.nats_scheme, '://', server])
                output, err = self.cli(server_to_connect)
                # print(output.stderr.decode('cp866'))
                if err != None:
                    output = json.loads(str(output))
                    servers_with_stream.append(servers_internal_names_ip[output['cluster']['leader']])
                    if 'replicas' in output['cluster'].keys():
                        for replica in output['cluster']['replicas']:
                            if 'name' in replica.keys():
                                servers_with_stream.append(servers_internal_names_ip[replica['name']])
            servers_with_stream = list(set(servers_with_stream))

            running_time = time.time() - start_time
            # default timeout_for_one_server_online = 60 seconds
            if running_time < self.timeout_for_one_server_online and len(servers_with_stream) == 0:
                time.sleep(self.timeout_for_one_server_online - running_time)
        return servers_with_stream

    def connect(self,servers_to_connect: list,servers_to_exclude={}) -> NATSClient:
        connected = False
        while not connected:
            start_time = time.time()
            random.shuffle(servers_to_connect)
            for server in servers_to_connect:
                # skip if server in failed state
                if server in [key for key in servers_to_exclude.keys() if servers_to_exclude[key] < time.time()]:
                    continue
                logger.info(f"Try to connect to {server}")
                client = NATSClient(url="".join([self.nats_scheme, '://', server]))
                try:
                    client.connect()
                    connected = True
                    logger.info(f"Connected {server}")
                    return client
                except Exception as e:
                    logger.info(f"Can't connect to {server}")
                    # mark server down for timeout_for_one_server_online seconds (default 60)
                    servers_to_exclude[server] = time.time() + self.timeout_for_one_server_online
            running_time = time.time() - start_time
            if running_time < self.timeout_for_one_server_online:
                time.sleep(self.timeout_for_one_server_online - running_time)

    def callback_debug(self, msg):
        logger.debug(f"{msg}")
