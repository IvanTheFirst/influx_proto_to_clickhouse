import sys
from threading import Thread
import logging
import signal
import time
from prod.nats_streaming import nats_streaming
from arguments import Configuration
from nats_to_clickhouse import ParserLoader
from pympler import asizeof

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

BUFFER = bytearray()

def callback_msg(msg):
    #logger.debug(msg.payload)
    BUFFER.extend(msg.payload)

def main():
    get_configuration = Configuration()
    (LOGLEVEL,
     readbufftime,dbname,dbsecure,dbhost,dbuser,dbpass,ttl,table_engine,chcluster,
     nats_servers,nats_scheme,path_to_nats_bin,client_name,server_stream,
     client_subject) = get_configuration.cmd_arguments()

    if LOGLEVEL == 'DEBUG':
        logger.setLevel(logging.DEBUG)
    elif LOGLEVEL == 'INFO':
        logger.setLevel(logging.INFO)
    elif LOGLEVEL == 'ERROR':
        logger.setLevel(logging.ERROR)
    elif LOGLEVEL == 'CRITICAL':
        logger.setLevel(logging.CRITICAL)
    elif LOGLEVEL == 'WARNING':
        logger.setLevel(logging.WARNING)
    else:
        logger.setLevel(logging.NOTSET)

    nats = nats_streaming(callback_msg = callback_msg,
                       loglevel = LOGLEVEL,
                       nats_servers = nats_servers,
                       nats_scheme = nats_scheme,
                       client_name = client_name,
                       server_stream = server_stream,
                       path_to_nats_bin = path_to_nats_bin,
                       client_subject = client_subject)

    stop_nats_thread = False
    nats_t1 = Thread(target=nats.start,args =(lambda : stop_nats_thread, ))
    nats_t1.start()

    clickhouse = ParserLoader(LOGLEVEL,dbname,dbsecure,dbhost,dbuser,dbpass,ttl,table_engine,chcluster)

    logger.info("Parser started")

    try:
        clickhouse.connect_to_db()
        clickhouse.get_db_scheme()
    except Exception as e:
        stop_nats_thread = True
        sys.exit(str(e))

    while True:
        start_time = time.time()
        new_line_last_index = BUFFER.rfind(b"\n")
        #logger.debug(new_line_last_index)
        if new_line_last_index > 0:
            element = BUFFER[:new_line_last_index]
            del BUFFER[:new_line_last_index]
            elements = element.splitlines()
            #logger.debug(f'to parse {len(elements)} ')
            try:
                clickhouse.parser(elements)
            #    clickhouse.writer()
            except Exception as e:
                stop_nats_thread = True
                sys.exit(str(e))
            logger.debug(f"Buffer: {asizeof.asizeof(BUFFER)}, clickhouse obj: {asizeof.asizeof(clickhouse)}, nats: {asizeof.asizeof(nats)}")
        running_time = time.time() - start_time
        if running_time < readbufftime:
            time.sleep(readbufftime - running_time)

if __name__ == "__main__":
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()
