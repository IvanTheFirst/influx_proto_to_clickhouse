## Что умеет
Умеет принимать протокол influx protocol по UDP и подключаться в JetStream в менеджере очередей NATS

## Как скомпилировать:

Для этого может потребоваться старый libffi6

LD_LIBRARY_PATH=packages/usr/lib/x86_64-linux-gnu:/data/files/langs/python/3.9.0/lib /data/files/langs/python/3.9.0/bin/pyinstaller --onefile prod/main.py --hidden-import cmath

## Пример настройки NATS JetStream
### Создание потока
nats str add TELEGRAF --subjects "TELEGRAF.*" --ack --max-msgs=-1 --max-bytes=-1 --max-age=10m --storage file --retention limits --max-msg-size=-1 --discard old --dupe-window="0s" --replicas 3

Важно обратить внимание на количество реплик, т.к. используется алгоритм RAFT, то меньше трёх быть не может. Если будет 2, то не заработает. То есть минимальное количество серверов тоже 3. Так же серверы в данной настройке не отчищают очередь, в случае если клиент получил сообщения. Все сообщения пишутся на диск, но есть вариант держать всё в памяти.

### создание клиента
nats con add TELEGRAF nats-python --ack none --target telegraf123 --deliver all --replay original --filter ''

Настройки --deliver all и --replay original позволяют получать все сообщения, пока клиент был офлайн и в том же порядке, в каком они поступили в очередь.
